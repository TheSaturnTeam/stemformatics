alter table assay_platforms rename column z_species to species;
alter table assay_platforms rename column z_platform_type to platform_type;
alter table assay_platforms rename column z_min_y_axis to min_y_axis;
alter table assay_platforms rename column z_y_axis_label to y_axis_label;
alter table assay_platforms rename column z_y_axis_label_description to y_axis_label_description;
alter table assay_platforms rename column z_mapping_id to mapping_id;
alter table assay_platforms rename column z_log_2 to log_2;
alter table assay_platforms rename column z_probe_name to probe_name;
alter table assay_platforms rename column z_default_graph_title to default_graph_title;

