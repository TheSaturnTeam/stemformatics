-- Story #153 add gene and probe columns as text to jobs table

ALTER TABLE stemformatics.users ADD COLUMN send_email_marketing boolean NOT NULL default False;
ALTER TABLE stemformatics.users ADD COLUMN send_email_job_notifications boolean NOT NULL default False;

