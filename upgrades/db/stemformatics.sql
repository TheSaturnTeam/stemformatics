-- Updates to Magma schema
-- TEMPORARY, UNTIL THESE CAN BE DEALT WITH APPROPRIATELY.

-- loosen constraints on probe_expressions fields
ALTER TABLE probe_expressions ALTER column detection DROP NOT NULL;
ALTER TABLE probe_expressions ALTER COLUMN detection DROP DEFAULT;
ALTER TABLE probe_expressions ALTER column rawscore DROP DEFAULT;
ALTER TABLE probe_expressions ALTER column normalizedscore DROP DEFAULT;
ALTER TABLE probe_expressions ALTER column normalizedlog2score DROP DEFAULT;
-- new chip_type field and indexes for improved query performance
ALTER TABLE probe_expressions ADD COLUMN chip_type integer;
ALTER TABLE probe_expressions ADD CONSTRAINT probe_expressions_chip_type_fkey FOREIGN KEY (chip_type) REFERENCES assay_platforms (chip_type) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION;
CREATE INDEX probe_expressions_chip_type ON probe_expressions (ds_id, chip_id, probe_id, chip_type);

-- OK: NOTE:
-- The Stemformatics project modifies this table to transform it from
-- row per transcript to row per gene, since we don't care about transcripts
-- for the purposes of annotation.
--
-- The original "magma" table is maintained here without changes and the
-- Stemformatics modification occurs in guide/model/stemformatics/db/stemformatics.sql

-- Remove "magma" table
DROP TABLE genome_annotations;

-- Create our modified version
CREATE TABLE genome_annotations (
    db_id INT REFERENCES annotation_databases (an_database_id) NOT NULL,
    Gene_ID TEXT NOT NULL,
    Description TEXT,
    Chromosome_Name TEXT,
    Gene_Start INT DEFAULT 0,
    Gene_End INT DEFAULT 0,
    Strand INT DEFAULT 0,
    Band TEXT,
    Associated_Gene_Name TEXT,
    Associated_Gene_Synonym TEXT,
    Associated_Gene_DB TEXT,
    Gene_Biotype TEXT,
    Source TEXT,
    Gene_Status TEXT,
    EntrezGene_ID TEXT,
    -- HGNC_ID for Human genome
    MGI_ID TEXT,
    RefSeq_DNA_ID TEXT,
    fts_lexemes TSVECTOR DEFAULT NULL,
    
    PRIMARY KEY (db_id, gene_id)
);
-- New indexes for modified table
CREATE INDEX gan_gene_id_ndx ON genome_annotations (Gene_ID);
CREATE INDEX gan_chr_name_ndx ON genome_annotations (Chromosome_Name);
CREATE INDEX gan_start_bp_ndx ON genome_annotations (Gene_Start);
CREATE INDEX gan_end_bp_ndx ON genome_annotations (Gene_End);
CREATE INDEX gan_name_ndx ON genome_annotations (Associated_Gene_Name);
CREATE INDEX gan_syn_ndx ON genome_annotations (Associated_Gene_Synonym);
CREATE INDEX gan_entrez_ndx ON genome_annotations (Entrezgene_ID);
CREATE INDEX gan_mgi_ndx ON genome_annotations (MGI_ID);
CREATE INDEX gan_refseq_ndx ON genome_annotations (RefSeq_DNA_ID);
CREATE INDEX gan_fts_ndx ON genome_annotations USING gin(fts_lexemes);



-- STEMFORMATICS --

CREATE SCHEMA stemformatics;

CREATE TABLE stemformatics.probe_expressions_avg_replicates
(
  ds_id integer NOT NULL,
  chip_type integer NOT NULL,
  chip_id text NOT NULL,
  probe_id text NOT NULL,
  replicate_group_id text NOT NULL,
  avg_expression double precision,
  standard_deviation double precision,
  detected boolean NOT NULL,
  ontolog text,
  sample_size integer DEFAULT 1,
  CONSTRAINT probe_expressions_avg_replicates_pkey PRIMARY KEY (ds_id, chip_type, chip_id, probe_id),
  CONSTRAINT probe_expressions_avg_replicates_chip_id_fkey FOREIGN KEY (chip_id, ds_id)
      REFERENCES biosamples (chip_id, ds_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT probe_expressions_avg_replicates_ds_id_fkey FOREIGN KEY (ds_id)
      REFERENCES datasets (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

CREATE INDEX detected_probes on stemformatics.probe_expressions_avg_replicates (probe_id, detected);
CREATE INDEX probes_detected_dataset on stemformatics.probe_expressions_avg_replicates (ds_id, chip_type, detected, probe_id);
CREATE INDEX ds_id_chip_type_probe_id ON stemformatics.probe_expressions_avg_replicates (ds_id, chip_type, probe_id);
CREATE INDEX probes_detected_dataset_other on stemformatics.probe_expressions_avg_replicates (detected, ds_id, chip_type, probe_id);
CREATE INDEX ontolog on stemformatics.probe_expressions_avg_replicates (ontolog);
CREATE INDEX group_id_probe_id on stemformatics.probe_expressions_avg_replicates (replicate_group_id, probe_id);


CREATE TABLE stemformatics.dataset_chip_type_map
(
 ds_id integer NOT NULL,
 chip_type integer NOT NULL,
 CONSTRAINT dataset_chip_type_map_pkey PRIMARY KEY (ds_id, chip_type),
 CONSTRAINT dataset_chip_type_map_assay_platforms_fkey FOREIGN KEY (chip_type)
     REFERENCES assay_platforms (chip_type) MATCH SIMPLE
     ON UPDATE NO ACTION ON DELETE NO ACTION,
 CONSTRAINT dataset_chip_type_map_datasets_fkey FOREIGN KEY (ds_id)
     REFERENCES datasets (id) MATCH SIMPLE
     ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE stemformatics.stats_detectedprobes_count
(
   ds_id integer NOT NULL,
   chip_type integer NOT NULL,
   probe_id text NOT NULL,
   detected integer NOT NULL DEFAULT 0,
   CONSTRAINT stats_detectedprobes_count_pkey PRIMARY KEY (ds_id, chip_type, probe_id),
   CONSTRAINT stats_detectedprobes_count_dsID_fkey FOREIGN KEY (ds_id)
       REFERENCES datasets (id) MATCH SIMPLE,
   CONSTRAINT  stats_detectedprobes_count_chipType_fkey FOREIGN KEY (chip_type)
       REFERENCES assay_platforms (chip_type) MATCH SIMPLE
);
CREATE INDEX stats_detectedprobes_count_ChipProbe_idx ON stemformatics.stats_detectedprobes_count(chip_type, probe_id);

CREATE TABLE stemformatics.stats_datasetsample_count
(
   ds_id integer NOT NULL,
   chip_type integer NOT NULL,
   samples integer NOT NULL,
   CONSTRAINT stats_samplesprobes_count_pkey PRIMARY KEY (ds_id, chip_type),
   CONSTRAINT stats_samplesprobes_count_dsID_fkey FOREIGN KEY (ds_id)
       REFERENCES datasets (id) MATCH SIMPLE,
   CONSTRAINT  stats_samplesprobes_count_chipType_fkey FOREIGN KEY (chip_type)
       REFERENCES assay_platforms (chip_type) MATCH SIMPLE
);

CREATE TABLE stemformatics.stats_genedetected
(
   database_id integer NOT NULL,
   identifier text NOT NULL,
   detected_samples integer NOT NULL,
   samples integer NOT NULL,
   CONSTRAINT stats_genedetected_pkey PRIMARY KEY (database_id, identifier),
   CONSTRAINT stats_genedetected_dbID_fkey FOREIGN KEY (database_id)
       REFERENCES annotation_databases (an_database_id) MATCH SIMPLE
);

-- New table to replace view "stemformatics.stats_datasetsumary"
CREATE TABLE stemformatics.stats_datasetsummary
(
   ds_id integer NOT NULL,
   md_name text NOT NULL,
   md_value text NOT NULL,
   count integer NOT NULL,
   CONSTRAINT stats_datasetsummary_pkey PRIMARY KEY (ds_id, md_name, md_value),
   CONSTRAINT stats_datasetsummary_dsid_fkey FOREIGN KEY (ds_id) REFERENCES datasets (id) MATCH SIMPLE
);


CREATE TABLE stemformatics.users
(
  uid SERIAL,
  username text NOT NULL,
  "password" text,
  organisation text,
  full_name text,
  status integer DEFAULT 0,
  created timestamp without time zone DEFAULT now(),
  confirm_code text,
  password_expiry timestamp without time zone,
  CONSTRAINT username PRIMARY KEY (username)
)
WITH (
  OIDS=FALSE
);

create unique index uid_unique on stemformatics.users (uid);

CREATE INDEX confirm_code
  ON stemformatics.users
  USING btree
  (confirm_code);

CREATE INDEX status
  ON stemformatics.users
  USING btree
  (status);



-- Story #123, uploading gene_sets
CREATE TABLE stemformatics.gene_sets
(
  id bigserial NOT NULL,
  gene_set_name text NOT NULL,
  db_id integer NOT NULL,
  uid integer NOT NULL,
  description text,
  CONSTRAINT pk_gene_sets PRIMARY KEY (id),
  CONSTRAINT unique_gene_sets UNIQUE (gene_set_name,uid)
)
WITH (
  OIDS=FALSE
);


CREATE TABLE stemformatics.gene_set_items
(
  id bigserial NOT NULL,
  gene_set_id bigint NOT NULL,
  gene_id text NOT NULL,
  CONSTRAINT pk_gene_set_items PRIMARY KEY (id),
  CONSTRAINT unique_gene_items UNIQUE (gene_set_id,gene_id)
)
WITH (
  OIDS=FALSE
);


-- Table: stemformatics.jobs

CREATE TABLE stemformatics.jobs
(
  job_id serial NOT NULL,
  analysis integer NOT NULL,
  status integer NOT NULL,
  dataset_id integer NOT NULL,
  gene_set_id integer NOT NULL,
  uid integer NOT NULL,
  created timestamp without time zone DEFAULT now(),
  finished timestamp without time zone default NULL,
  use_cls boolean,
  use_gct boolean,
  reference_type text,
  reference_id text,
  gene text,
  probe text,
  comparison_type text,
  CONSTRAINT job_id PRIMARY KEY (job_id)
)
WITH (
  OIDS=FALSE
);

-- NOTE: This could break if target DB user not 'ascc_dev'
ALTER TABLE stemformatics.jobs OWNER TO ascc_dev;


CREATE TABLE stemformatics.feature_mappings (
    db_id INT REFERENCES annotation_databases (an_database_id) NOT NULL,
    chip_type INT REFERENCES assay_platforms (chip_type) NOT NULL,
    from_type TEXT NOT NULL,
    from_id TEXT NOT NULL,
    to_type TEXT NOT NULL,
    to_id TEXT NOT NULL,
    PRIMARY KEY (db_id, chip_type,from_type,from_id,to_type,to_id)
);

CREATE INDEX to_index on stemformatics.feature_mappings (db_id,chip_type,to_type,to_id);
    



CREATE TABLE stemformatics.transcript_annotations
(
   db_id INT REFERENCES annotation_databases (an_database_id),
   transcript_id text NOT NULL,
   gene_id text NOT NULL,
   transcript_name text NOT NULL,
   transcript_start integer NOT NULL,
   transcript_end integer NOT NULL,
   protein_length integer NOT NULL default 0,
   signal_peptide boolean default False,
   tm_domain boolean default False,
   targeted_miRNA boolean default False,
   CONSTRAINT transcript_annotations_pkey PRIMARY KEY (db_id, transcript_id)
);


CREATE INDEX transcript_annotations_db_id_gene_id ON stemformatics.transcript_annotations (db_id, gene_id);


-- Story #159 for saving filters for annotations
CREATE TABLE stemformatics.annotation_filters
(
   id serial NOT NULL,
   uid INT NOT NULL,
   name text NOT NULL,
   json_filter text NOT NULL,
   CONSTRAINT annotation_filters_pkey PRIMARY KEY (id)
);

CREATE INDEX annotation_filters_uid ON stemformatics.annotation_filters (uid);



ALTER TABLE stemformatics.transcript_annotations ADD COLUMN size integer NOT NULL default 0;

update stemformatics.transcript_annotations set size = transcript_end - transcript_start;




-- ensure gene set items has index on gene id
CREATE INDEX gene_set_items_gene_id ON stemformatics.gene_set_items (gene_id);


-- have to remove transcript start/end/size details as they are useless
alter table stemformatics.transcript_annotations drop column transcript_start;
alter table stemformatics.transcript_annotations drop column transcript_end;


-- gene sets adding the type 
ALTER TABLE stemformatics.gene_sets ADD COLUMN gene_set_type text NOT NULL default '';



-- speeding up the annotation page
CREATE INDEX gene_sets_uid_id ON stemformatics.gene_sets (uid,id);
