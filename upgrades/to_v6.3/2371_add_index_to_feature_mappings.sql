CREATE INDEX to_idx ON stemformatics.feature_mappings (to_type,to_id,from_type);


CREATE INDEX db_id_idx ON datasets (db_id);
CREATE INDEX private_idx ON datasets (private);
CREATE INDEX chip_type_idx ON datasets (chip_type);
