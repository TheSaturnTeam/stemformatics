#!/bin/bash

cd /data/repo/git-working/stemformatics/upgrades/to_v6.7/
./2727_collapse_add_glimma_buttons.sh


psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v6.7' where ref_type = 'stemformatics_version';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'true' where ref_type = 'use_cdn';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'stemformatics.sa.metacdn.com/release_67' where ref_type = 'cdn_base_url';"

wget http://localhost:5000/api/trigger_config_update -O /dev/null


echo "Now you will need to upgrade the help to v6.7"
