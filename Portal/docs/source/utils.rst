.. utilities section

Utilities
=========

Classes and functions described in this section can be accessed using

.. code-block:: python
 
 >>> from guide.lib import *


.. toctree:: 
   :maxdepth: 2

.. automodule:: sluice
    :members:

.. automodule:: ppmc
    :members:
