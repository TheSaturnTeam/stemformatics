.. configuration section

=================
Configuring GuIDE
=================

.. _project-directory:

Project Directory
"""""""""""""""""

All of GuIDE's files, with the possible exception of those related directly to 
any DBMSs that it uses, are found in the file system tree located under and within
the ``project directory``.  
By convention, the :envvar:`GUIDEPATH` environment variable
is set to this directory path and is referenced from various modules and
configuration files within GuIDE.

.. _authkit-and-openid:

Authkit and OpenID
""""""""""""""""""

The web portal implementation of GuIDE provides user account management that
includes login credentials based on either username+password identification,
or `OpenID <http://openid.org/>`_.   These settings can all be found 
in the project configuriation file (``development.ini``, ``test.ini``, or ``production.ini``) 
located in the top level of the :ref:`project-directory`.

Either of these schemes require certain basic settings to 
enable them within the GuIDE/Pylons framweork.  

.. _authkit-basic-setup:

AuthKit Basic Session Settings
""""""""""""""""""""""""""""""

These parameters tell AuthKit what time of authentication it will provide (OpenID, here)
and where it will keep session context data for each user session.  Unless there is some
dire need to change them, all but the ``secret`` parameters can and should be left as they are.
Change the ``secret`` settings to whatever seems appropriate or leave them as they are. 
In any event, do not make them blank as this will either prevent AuthKit from working properly
or leave the client vulnerable to a session hijacking attack.

::

   authkit.setup.method = openid, cookie
   authkit.cookie.secret = chocolate_bisquit_day
   authkit.openid.session.middleware = beaker.session
   authkit.openid.session.key = authkit_openid
   authkit.openid.session.secret = nowayhozay
   authkit.intercept = 401

.. _authkit-paths:

AuthKit will need to construct the URI to which the user will be redirected once
their login attempt is successful.  The ``signedin`` parameter provides the path
part of the URI.  (The method and host are provided in the :ref:`authkit-base-url` 
parameter.)

To sign the user out, provide them a link that uses the ``signoutpath`` parameter
for the path part of the URI. 

::

   authkit.openid.baseurl = http://haematlas.wehi.edu.au/
   
   authkit.openid.path.signedin = /main/index
   authkit.cookie.signoutpath = /main/logout


This is where OpenID will keep incoming, "current" information
about authenticated users.

::

   authkit.openid.store.type = file
   authkit.openid.store.config = %(here)s/data/openid.db

.. _login-template:

Login Template
""""""""""""""

AuthKit does not easily support any of Pylons' templating languages (e.g. 
`mako <http://makotemplates.org/>`_) so the choice is to either specify
a string here in the config file, a text file (not a template!), or a 
reference to an executable object.  (The latter is a sort of wonky way 
to invoke a mako template, but doesn't always work properly.)  

::

   authkit.openid.template.file = %(here)s/guide/templates/GuideLogin.pys

Several
substitution variables must be used in this templ... ooops, I mean string/file/object
for it to work properly:

:token:`$action`
   should go in the ``action='$action'`` part of a ``<form>`` tag
:token:`$css_class`
   goes in the ``<div class='...'>`` wrapper for the form
:token:`$message`
   where authkit will put error / info messages
:token:`$value` 
    value='$value' part of ``<input ...>`` tag
    


.. _autkit-userinfo:

OpenID User Information
"""""""""""""""""""""""

We can tell the verifier to pass along other information, via supplemental
registry (SREG) or authentication extentions (AX) settings.   AX is the newer
of the two standards and though nearly all support both of them, Google in 
particular really only supports this one.

::

   # SREG ...
   authkit.openid.sreg.required = fullname,email
   authkit.openid.sreg.optional = dob,country,tz
   
   # AX ...
   authkit.openid.ax.typeuri.email = http://axschema.org/contact/email
   authkit.openid.ax.typeuri.namePerson = http://axschema.org/namePerson
   authkit.openid.ax.alias.email = email
   authkit.openid.ax.alias.namePerson = namePerson
   authkit.openid.ax.required.email = true
   authkit.openid.ax.required.namePerson = true


.. _portal-name:

Application Apellation
""""""""""""""""""""""

Portal application name that will be used throughout the site as default title e.g. in web templates
abacus.application.name = "ASCC Portal Framework"

.. _abacus-database:

ABACUS Database URI
"""""""""""""""""""

The ABACUS database URI should be a URL that can be passed to
sqlalchemy's "create_engine" function.  Note that unless a trust
relationship is configured between the pylons UID and the dbms
any passwords required must be encoded in the url, which means this
(.ini) file must be protected to avoid compromise of those login
credentials.  (Applies to PostgreSQL, MySQL, Oracle, etc.  Does
not apply to SQLite, e.g.)

::

   abacus.user.db.url = postgresql://guidedb@localhost:1234/abacus

.. _credential-location-and-priority:

Credential Location and Priority
""""""""""""""""""""""""""""""""

The user's credentials are presented by either (or both) of two
different environment elements:  the :envvar:`REMOTE_USER` environment variable
or the ``Authorization:`` HTTP header.  When both are present, this
configuration parameter indicates which to choose.
If set to :token:`header` this will
give the header preference, i.e. the :envvar:`REMOTE_USER` environment
variable will be ignored.  It can also be set to :token:`environ` in which
case the http Authorization: header will be ignored.  Either of these 
assume that both are set.

::

   abacus.user.id.priority = header

The :token:`ignore` parameter is used to enable or disable reference to the two elements 
that are referenced when identifying a user.  It can be set to 
:token:`both`, 
:token:`neither`, 
:token:`header`, or 
:token:`environ`.  
Setting this
to "both" will effectively disable id checking, allowing anyone
to access the site.

::

   abacus.user.id.ignore = neither

.. _validation-mode:

Validation Mode
"""""""""""""""

User login validation pathways can vary depending on how this parameter is set.  
The authentication module (ABACUS) is designed to collect and maintain ancillary
user information (non-authentication data such as full name or email address)
and can do so when the parameter is set to :token:`register`.  Once a user has 
registered, their account status will be set to the value of 
`abacus.user.registration.default_status`_.  If this is :token:`PENDING`, their access to the
GuIDE will either be limited or denied altogether until a site administrator
changes this to ``ACTIVE``.  To open the site up to any and all users, completely
disabling all authentication, set this parameter to :token:`anonymous`.

::

   abacus.user.id.validation.mode = register

:token:`full`
   allow only registered users to access the site
:token:`register`
   allow (possibly limited) access after completing a registration process that collects fullname and email addr.
:token:`anonymous`
   allow anyone in with openID.

Note that ``abacus.user.id.priority`` and ``abacus.user.id.ignore`` may
supercede any effect this option may have.
   
   
.. _abacus.user.registration.default_status:

Default Registration Status
"""""""""""""""""""""""""""

When a user registers they may or may not be allowed to immediately proceed to 
the site proper.   By default, the user's account is in a ``PENDING`` state until
a site adminstrator changes it either to ``ACTIVE`` (or some other) state.  The
``default_status`` parameter tells GuIDE (ABACUS) what the default state should be
following registration.   Setting this to ``ACTIVE`` will allow a newly-registered user
to access the site without further intervention by the site administrator.

::
   
   # Set to ACTIVE to allow immediate use of the site following
   # registration:
   abacus.user.registration.default_status = PENDING

Portal Email
""""""""""""

The ``to`` parameter should be a comma-delimited list of addresses of the site
administrator(s).  The ``from`` parameter can be any valid email address, although
it should, strictly speaking, be an address that is also a valid email address
from which emails can be sent or relayed with the sites operational environment. 
(I.e., don't make it no@body.com as this will probably prevent outgoing emails from
making it through the MTA responsible for sending the site's emails.)

::

   abacus.message.email.from = guideadmin@wehi.edu.au
   abacus.message.email.to = seidenman@wehi.edu.au, jchoi@wehi.edu.au

