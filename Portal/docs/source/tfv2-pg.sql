-- =============================================================================
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- =============================================================================
--
-- tfv-pg.sql
--
-- PostgreSQL translation of Mattew Anderson's  TFV (transcript framework viewer) 
-- database schema as provided 14 June 2010.  Matt's version was written for
-- a MySQL implementation.
--
-- In addition to translation to pgsql, this version also contains a number of
-- changes to Matt's original schema, including the addition of several tables
-- and sequences to supportl dynamic enumerated types.
--
-- One major change is in the way indexes are defined.  MySQL allows them
-- to be defined within the table definition, almost as though they were
-- just another column or column constraint.  Indexes are defined outside
-- of (and AFTER) the tables that they're indexing.
--
-- It also contains additional functions and triggers that handle database 
-- column transformations (typically on insertions) and integrity checks.
--
-- Although this is more or less specific to PostgreSQL, I've tried to keep 
-- the syntax as close to SQL95 as possible, in anticipation of the need to
-- someday, perhaps, migrate this yet again to some other DBMS such as sqlite.
-- Having said this, it is worth noting that I was using postgres 8.4.3 
-- at the time I was writing this, since we have found irksome differences
-- (read: features missing) between this and earlier versions of postgresql
-- even as recent as 8.1.
--
-- I have also added comments explaining changes or additions, probably to a 
-- greater extent than is really needed, but ... better safe than sorry.  I'd like
-- to think that someone else will someday be maintaining this.  NLS
--
-- Nick Seidenman <seidenman@wehi.edu.au>
-- Matthew Anderson <mattay@griffiths.edu.au> (original MySQL schema)
--
-- =============================================================================

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Matt's version defined a separate schema and I can see no reason not to 
-- retain this, for the moment.  It may be that we'll just put this in a
-- separate database altogether 
DROP SCHEMA TFV CASCADE;
CREATE SCHEMA  TFV;
SET SEARCH_PATH TO TFV;

DROP TABLE gender_enum;
DROP TABLE species_enum;
DROP TABLE feature_type;
DROP SEQUENCE type_seq;

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Set up a few "enum" tables and sequences that we'll
-- need later on.
--
-- These appeared as ENUM types in the MySQL version.  Although postgres
-- supports a similar construct, I prefer to use this one as it allows
-- new enumerands to be added without having to modify the schema itself,
-- and also provides a simple method for integrity checking (values
-- not found in the enumeration.)
--
-- Using tables to simulate enums here.  This let's us
-- add or modify the enum range without having to 
-- do an "alter table".  Just add, update, or remove
-- the appropriate rows in this table.  
--
-- BEWARE that
-- this will have foreign key consequences in the
-- referring tables!

CREATE TABLE TFV.gender_enum (
       label TEXT PRIMARY KEY
);

INSERT INTO TFV.gender_enum VALUES ('Unkown'); 
INSERT INTO TFV.gender_enum VALUES ('Male'); 
INSERT INTO TFV.gender_enum VALUES ('Female');  
INSERT INTO TFV.gender_enum VALUES ('XX'); 
INSERT INTO TFV.gender_enum VALUES ('XY'); 

CREATE TABLE TFV.species_enum (
       label TEXT PRIMARY KEY
);

INSERT INTO TFV.species_enum VALUES ('Human');
INSERT INTO TFV.species_enum VALUES ('Mouse');
INSERT INTO TFV.species_enum VALUES ('Drosophilla');


-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- This originally appeared as chip_type INT(11) AUTOINCREMENT
-- in Matt's schema.  PostgreSQL has a corresponding qualifier (serial)
-- but using an external sequence provides a bit better control 
-- and referencing options, so I prefer to use that instead.
--
-- The INCREMENT BY 32 is an artifact of the GUI Matt used to create this
-- schema.  There's no particular need for it, but I've retained it to 
-- in the unlikely event that it should actually matter somehow.
--
-- In later incarnations of this I needed sequences for other columns
-- in other tables.  Since there's no intrinsic meaning in the
-- sequence values, there's no reason not to use a single sequence
-- for these columns, so I changed chip_type_seq to type_seq and
-- changed the increment to 5.

CREATE SEQUENCE type_seq
       INCREMENT BY 5;

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.assay_platforms
--
-- Other than chip_type, nearly identical to MySQL schema.
DROP TABLE IF EXISTS TFV.assay_platforms ;

CREATE TABLE TFV.assay_platforms (
  chip_type INT DEFAULT nextval('type_seq') PRIMARY KEY,
  species TEXT NOT NULL REFERENCES species_enum(label),
  platform TEXT NULL DEFAULT NULL ,
  ap_type TEXT NULL DEFAULT NULL ,
  version TEXT NULL DEFAULT NULL 
);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.annotation_databases
--
-- This did no appear in the original (MySQL) version.  It was added later
-- to reduce the number of columns required in other tables for primary
-- and/or foreign keys.  NLS 20100707

DROP TABLE IF EXISTS TFV.annotation_databases;

CREATE TABLE TFV.annotation_databases (
  an_database_id INT PRIMARY KEY DEFAULT nextval('type_seq'),
  genome_version TEXT NOT NULL,
  annotator TEXT NOT NULL,
  annotation_version TEXT NOT NULL,
  model_id TEXT NOT NULL
);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.genome_annotations
--
-- This table is used in conjunction with
-- genome_mappings to annotate a gene including all of
-- it's synomyms as well as the various IDs found in
-- their respective databases (e.g. ensembl & entrez).

DROP TABLE IF EXISTS TFV.genome_annotations ;

CREATE TABLE TFV.genome_annotations (
  database_id INT NOT NULL REFERENCES TFV.annotation_databases(an_database_id),
  identifier TEXT NOT NULL DEFAULT '' ,
  chromosome TEXT NULL DEFAULT NULL ,
  strand INT DEFAULT NULL ,
  start_bp INT  DEFAULT NULL ,
  end_bp INT  DEFAULT NULL ,
  PRIMARY KEY (database_id, identifier) );

CREATE INDEX TFV_locii_id_ndx ON TFV.genome_annotations (identifier);
CREATE INDEX TFV_locii_chr_ndx ON TFV.genome_annotations (chromosome);
CREATE INDEX TFV_locii_sbp_ndx ON TFV.genome_annotations (start_bp);
CREATE INDEX TFV_locii_ebp_ndx ON TFV.genome_annotations (end_bp);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.genome_mappings
--
-- As of the first (MySQL) version of this schema there was no specific
-- indication as to whether there would be a domain -> range relationship
-- between these two "sides" and, if so, which was which.  I decided to
-- establish a "rule" that "side 1" would always hold the ensembl annotation
-- which would always point to a record in the genome_annotations table.
-- Thus, a foreign key reference is appropriate for this side.   Side 2
-- will be used to store other annotations, gene symbol(s) and synonyms.
-- Since these will never exist in a specified (i.e. fixed) column 
-- in the genome_annotations table, there can be no foreign key constraint.
-- Hence, I have removed it for side 2.
--
--   +----------------------+---------------------+
--   |   global genome info (version, model ID)   |
--   +----------------------+---------------------+
--   |   annotator1 info    |  annotator2 info    |
--   |  (annot8r, vers, id) | (annot8r, vers, id) |
--   +----------------------+---------------------+
--
-- To be consistent with the database_id concept (see annotation_databases,
-- above) the genome_version / annotator version ... columns have been replaced
-- with a database_id that uses a FK reference to maintain integrity.  This
-- will make it easier to use this table for other FK references to it.
--
-- The table now looks more like this:
--
--   +----------------------+---------------------+
--   |   annotator1 info    |  annotator2 info    |
--   | (dbID, ens gene ID)  |  (dbID, symbol)     |
--   +----------------------+---------------------+
--
-- Searching on annotator_id2 will return 0 or more columns with the
-- results of the annotator*1 columns pointing to the (for now) Ensembl
-- genome information.  Searching on ann*1 with Ensembl gene ID will
-- return associated symbols, synomyms and other accession data for
-- this gene in other databases.

DROP TABLE IF EXISTS TFV.genome_mappings ;

CREATE TABLE TFV.genome_mappings (
  annotator_db1 INT REFERENCES TFV.annotation_databases(an_database_id),
  annotator_id1 TEXT NOT NULL DEFAULT '' ,
  annotator_db2 INT REFERENCES TFV.annotation_databases(an_database_id),
  annotator_id2 TEXT NOT NULL DEFAULT '' ,
  -- Not sure if we'll really need this, but to keep SQLAlchemy's object mappers
  -- happy, gotta have it.  As this table will only be used for M:M relationships,
  -- we may well end up removing it sooer or later.  NLS
  fspk SERIAL PRIMARY KEY
);

CREATE INDEX genome_mappings_id1_ndx ON TFV.genome_mappings(annotator_id1);
CREATE INDEX genome_mappings_id1_ndx ON TFV.genome_mappings(annotator_id2);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.probe_annotations

DROP TABLE IF EXISTS TFV.probe_annotations ;

CREATE TABLE TFV.probe_annotations (
  chip_type INT NOT NULL DEFAULT 0 ,
  probe_id TEXT NOT NULL DEFAULT '0' ,
  probe_sequence TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (chip_type, probe_id) ,
  CONSTRAINT probe_annotations_ibfk_1
    FOREIGN KEY (chip_type )
    REFERENCES TFV.assay_platforms (chip_type )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE INDEX probe_annotations_chip_type ON TFV.probe_annotations(chip_type);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.probe_mappings

DROP TABLE IF EXISTS TFV.probe_mappings ;

CREATE TABLE TFV.probe_mappings (
  chip_type INT NOT NULL DEFAULT 0 ,
  probe_id TEXT NOT NULL DEFAULT '0' ,
  database_id INT REFERENCES TFV.annotation_databases(an_database_id),
  identifier TEXT NOT NULL ,
  mapped INT DEFAULT NULL, -- COMMENT percentage of probe sequence mapped to annotation. ,
  PRIMARY KEY (chip_type, probe_id, database_id, identifier),
  FOREIGN KEY (chip_type , probe_id ) REFERENCES TFV.probe_annotations (chip_type , probe_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  FOREIGN KEY (database_id, identifier) REFERENCES TFV.genome_annotations (database_id, identifier)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE INDEX probe_mappings_id_ndx ON TFV.probe_mappings(identifier);

-- =============================================================================
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- =============================================================================
--
-- Everything below this ^^^ line is currently unused in the WEHI implementation
-- and may, for now, be ignored.  I put it here to keep the parts that are being
-- used distinct and relatively free of extraneous definitions.  NLS 
\echo > > > > > > > > >   BEGIN UNUSED DEFINITIONS   < < < < < < < < < <

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- TABLE TFV.genelist_collections
-- (WEHI / MMD doesn't really use genelists, per se, so this table sits empty 
-- and unused for now.)

DROP TABLE IF EXISTS TFV.genelist_collections ;

CREATE TABLE TFV.genelist_collections (
  genelist_id INT NOT NULL REFERENCES TFV.genelist_descriptions (genelist_source_id)
    ON DELETE NO ACTION ON UPDATE NO ACTION,
  database_id INT REFERENCES TFV.annoation_databases(an_database_id),
  identifier TEXT NOT NULL
);

CREATE INDEX genelist_collections_gene_id_ndx ON TFV.genelist_collections(identifier);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.publications
-- 
-- Nearly identical to MySQL schema.  (We aren't really using this
-- at the moment anyway and as of 30 June 2010 is always an empty table.)

DROP TABLE IF EXISTS TFV.publications ;

CREATE TABLE TFV.publications (
  title TEXT NOT NULL ,
  abstract TEXT NULL DEFAULT NULL ,
  authors TEXT NULL DEFAULT NULL ,
  main_authors TEXT NULL DEFAULT NULL ,
  publisher TEXT NULL DEFAULT NULL ,
  archive_host TEXT NULL DEFAULT NULL ,
  archive_accession_id TEXT NOT NULL DEFAULT '' ,
  url_source TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (archive_accession_id) );


-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.biosamples
--
-- This corresponds roughly to our sample data, but since we (WEHI / MMD) 
-- are more or less mouse- (and blood- ) specific, the concepts of tissue,
-- gender, and age are more or less meaningless to us.  Just sayin' ...

DROP TABLE IF EXISTS TFV.biosamples ;

CREATE TABLE TFV.biosamples (
  archive_accession_id TEXT NOT NULL DEFAULT 'none' ,
  chip_type INT NOT NULL DEFAULT 0 ,
  chip_id TEXT NOT NULL DEFAULT '0' ,
  sample_id TEXT NOT NULL ,
  replicate INT NOT NULL DEFAULT 0 ,
  tissue TEXT DEFAULT 'Unknown' ,
  cell TEXT  DEFAULT 'Unknown' ,
  status TEXT DEFAULT 'Unknown' ,
  gender TEXT DEFAULT 'Unknown' REFERENCES gender_enum(label),
  age INT NULL DEFAULT 0 ,
  PRIMARY KEY (chip_id, chip_type) ,
  CONSTRAINT biosamples_ibfk_1
    FOREIGN KEY (archive_accession_id )
    REFERENCES TFV.publications (archive_accession_id )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT biosamples_ibfk_2
    FOREIGN KEY (chip_type )
    REFERENCES TFV.assay_platforms (chip_type )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE INDEX biosamples_accession ON TFV.biosamples(archive_accession_id, chip_type, chip_id);
CREATE INDEX biosamples_chip_type ON TFV.biosamples(chip_type);


-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.genelist_annotators
--
-- Name a genelist and specify its source.   (WEHI / MMD doesn't really use
-- genelists, per se, so this table sits empty and unused for now.)

DROP TABLE IF EXISTS TFV.genelist_annotators ;

CREATE TABLE TFV.genelist_annotators (
  genelist_id INT NOT NULL ,
  name TEXT NOT NULL ,
  source TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (genelist_id) );


-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.genelist_descriptions
-- 
-- (WEHI / MMD doesn't really use
-- genelists, per se, so this table sits empty and unused for now.)


DROP TABLE IF EXISTS TFV.genelist_descriptions ;

CREATE TABLE TFV.genelist_descriptions (
  genelist_source_id INT NOT NULL ,
  genelist_id INT NOT NULL ,
  description TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (genelist_source_id) ,
  CONSTRAINT genelist_descriptions_ibfk_1
    FOREIGN KEY (genelist_source_id )
    REFERENCES TFV.genelist_annotators (genelist_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE INDEX genelist_descriptions_genelist_source ON TFV.genelist_descriptions(genelist_source_id);

-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.probe_expressions

DROP TABLE IF EXISTS TFV.probe_expressions ;

CREATE TABLE TFV.probe_expressions (
  chip_type INT NOT NULL DEFAULT 0 ,
  chip_id TEXT NOT NULL DEFAULT '0' ,
  probe_id TEXT NOT NULL DEFAULT '0' ,
  avg_signal DOUBLE PRECISION  DEFAULT NULL ,
  quality_type TEXT  DEFAULT NULL ,
  quality_value DOUBLE PRECISION  DEFAULT NULL ,
  normalized_signal DOUBLE PRECISION  DEFAULT NULL ,
  PRIMARY KEY (chip_type, chip_id, probe_id) ,
  CONSTRAINT probe_expressions_ibfk_1
    FOREIGN KEY (chip_type , chip_id )
    REFERENCES TFV.biosamples (chip_type , chip_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT probe_expressions_ibfk_2
    FOREIGN KEY (chip_type , probe_id )
    REFERENCES TFV.probe_annotations (chip_type , probe_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE INDEX probe_expressions_chip_type_2 ON TFV.probe_expressions(chip_type, probe_id);
CREATE INDEX probe_expressions_chip_type ON TFV.probe_expressions(chip_type);
CREATE INDEX probe_expressions_expression ON TFV.probe_expressions(chip_type, chip_id, probe_id);
CREATE INDEX probe_expressions_probe_id ON TFV.probe_expressions(probe_id, chip_type);
CREATE INDEX probe_expressions_chip_id ON TFV.probe_expressions(chip_id);


-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
-- Table TFV.sample

DROP TABLE IF EXISTS TFV.sample ;

CREATE TABLE TFV.sample (
  sample_id INT NOT NULL ,
  replicate TEXT DEFAULT NULL ,
  tissue TEXT  DEFAULT NULL ,
  cell TEXT  DEFAULT NULL ,
  status TEXT  DEFAULT NULL ,
  sex TEXT  DEFAULT NULL ,
  age TEXT  DEFAULT NULL ,
  PRIMARY KEY (sample_id) 
);

\echo > > > > > > > > >    END UNUSED DEFINITIONS    < < < < < < < < < <
