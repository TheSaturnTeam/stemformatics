
================================================
Building the Pylons / Portal Runtime Environment
================================================

Introduction
------------

Herein are described the steps for building an environment in which a pylons/postgresql
application can be run.    The advantage to the approach used here
is that it can be done entirely with ordinary user (i.e. no root or Administrator) access.   
Two key components -- `python` and `postgresql` -- from sources.  
This may sound scary if you've never done it before, but it really isn't.  Just follow the steps, below.  

Note: this assumes you're using the :ref:`bash` shell, but the commands are pretty much the same for the 
:ref:`Bourne Shell` (``sh``) or the :ref:`Korn Shell` (``ksh``).  For :ref:`C Shell` (``csh``) all bets are off.

Basic Stuff
-----------

1. Create a directory under your home directory called "root".  (If you already have one, pick another name.)

.. code-block:: bash

     $ cd ~
     $ mkdir root


2. Create another directory under your home directory called "src".

.. code-block:: bash

   $ mdkir src

3. Create an environment variable called ``MYROOT`` and set it to the ``root`` directory
you created in :ref:`Basic Stuff`.

.. code-block:: bash

   $ export MYROOT=$HOME/root

You'll want to add this line to your ``.profile`` or ``.bashrc`` file so that it sets this variable
each time you log in or start up a new shell.

4. Change your working directory to the ``$MYROOT/src`` directory.   Double-check that you're in the right place (i.e. that 
you've set the ``MYROOT`` environment variable correctly.

.. code-block:: bash

   $ cd $MYROOT/src    
   $ pwd    # Result should be the same as $HOME/root/src

Building Python
---------------

5. If you don't want to build and install your own instance of :ref:`python` you can check to see if there's
one already on your system and if it's the right version this way:

.. code-block:: bash

   $ python --version
 
You should see it display a version number that is 2.6.3 or later.   The latest version 2 release is 2.7.

6. If you do want to build python from source, go to the :ref:`python web site <http://www.python.org/>`
and download the source for the latest version.  You can get the 
:ref:`2.7 sources here <http://www.python.org/ftp/python/2.7/Python-2.7.tar.bz2>` using your browser,
or you can use :ref:`curl` (or :ref:`wget`) to get it directly:

.. code-block:: bash

   $ curl -O "http://www.python.org/ftp/python/2.7/Python-2.7.tar.bz2" 
   # alternatively:  wget "http://www.python.org/ftp/python/2.7/Python-2.7.tar.bz2"

If you download it with your browser, odds are that it'll put the file someplace other than your 
``$MYROOT/src`` directory, so once the download has finished, move the file there.

7. Download :ref:`PostgreSQL` sources by going to the :ref:`postgres site <http://www.postgresql.org/>`
and getting the latest sources.  As of this writing, the current version is 9.0.1.  Feel free to get a 
later version, however, *make sure you download nothing earlier than 8.4.3*.  Here's a link to the download
page.  (Unfortunately, the way they have their mirrors set up it's difficult to provide a direct link.)

.. code-block:: bash

    http://wwwmaster.postgresql.org/download/mirrors-ftp/source/v9.0.1/postgresql-9.0.1.tar.bz2

Once the download has finished, move this to the ``$MYROOT/src`` directory, too.

8. Make sure you're still in the ``$MYROOT/src`` directory and unpack these two files:

.. code-block:: bash

   $ tar xjf Python-2.7.tar.bz2
   $ tar cjf postgresql-9.0.1.tar.bz2

You'll see no output and each command will take less than a minute.  When this has finished, you
should have two new subdirectories under ``$MYROOT/src``:  ``Python-2.7`` and ``postgresql-9.0.1``.

9. ``CD`` into ``Python-2.7`` and begin the build.  To do this, you'll run a command called ``configure``
and tell it where the executables and libraries will go.

.. code-block:: bash

   $ cd Python-2.7
   $ ./configure --prefix=$MYROOT

The ``prefix`` switch tells the configuration script where the *root* directory is for this installation.
this normally defaults to /usr/local, however, we'd need root (or Administrator) access to install things there
so we'll tell it the root is really in our own environment.

10.  The ``configure`` script will spit out plenty of messages the last of which should look something like this:

.. code-block:: bash

  configure::6056:: checking for thread.h
  configure::6056:: result: no



When it's finished, you should have a ``Makefile`` in your current directory and you'll now be ready to
compile and install python.

11. Build `python` by simple running ``make`` :

.. code-block:: bash

   $ make

This will produce LOTS more output, telling you all sorts of things about how the build is progressing.   If this
finishes successfully (and it *should* ), you can then install the newly-built interpreter and its libraries
by executing

.. code-block:: bash

   $ make install

After lots more output, telling you where it's putting all this good stuff, python will have been built
and installed into your new root (i.e. ``$MYROOT`` ) environment.  Test this by running

.. code-block:: bash

   $ $MYROOT/bin/python
 
    Python 2.7 (r27:82500, Oct  7 2010, 17:51:51) 
    [GCC 4.2.1 (Apple Inc. build 5664)] on darwin
    Type "help", "copyright", "credits" or "license" for more information.
    >>> 

then type ctrl-D to exit the interpreter.  Take notice of the date and time in the first line of this output.
This should be pretty close to the date and time -- within minutes, if you're doing this
right after a build/install has finished -- when this particular python executable was built.

12.  You should still be in the $MYROOT/src/Python-2.7.  Go up two  directories (that is, back to 
``$MYROOT/src``) and execute ``ls`` ...
.. code-block:: bash

   $ cd ..
   $ ls

You should now see several other directories, including a ``bin`` and a ``lib`` .   You should now set your
``PATH`` environment variable to include ``$MYROOT/bin`` so that when you run ``python`` the shell will
run `this` python and not the system's python.

.. code-block:: bash

   $ export PATH="$MYROOT/bin:$PATH"

This will add ``$MYROOT/bin`` as the first directory scanned so that your new python is picked up, rather than the
system python, if there is one.   Verify that you've done this correctly by just typing ``python`` at the
shell prompt and look at the first line of the output.  This should be *exactly* the same as the
output you saw when you ran python with the explicit path in step 11, above.

.. code-block:: bash

   $ python
   Python 2.7 (r27:82500, Oct  7 2010, 17:51:51) 
   [GCC 4.2.1 (Apple Inc. build 5664)] on darwin
   Type "help", "copyright", "credits" or "license" for more information.
   >>> 

(Type ctrl-D to quit.)

You should also add this line to your``.profile`` or ``.bashrc`` file
so that it makes this alteration to your ``PATH`` each time a new shell is started.

Building PostgreSQL
-------------------

12. Now build postgresql.  Change into the postgres source directory that you unpacked earlier and do
pretty much the same things you did to build python ...

.. code-block:: bash

   $ cd $MYROOT/src/postgresql-9.0.1
   $ ./configure --prefix=$MYROOT
   $ make
   $ make install

Once this has all finished, you'll have postgres binaries and libraries installed in  your ``$MYROOT``
directory.  

Creating a PostgreSQL Instance
------------------------------

13.  Before initializing the postgres runtime environment you'll want to set a few more environment variables
to avoid having to type certain things -- like the server's port number or the path where it puts
database files (including configurations.)

.. code-block:: bash

   $ export PGDATA=$MYROOT/pgdata
   $ export PGPORT=5433

You can make the port an unused port, however postgres will default to 5433 and this is also what
we use in the ASCC portal application.  If you're using a different port, set PGPORT to that number instead.
As with the other environment variables, it's a very good idea to put these commands in your ``.profile``.

14.  Before you can start up the postgres server you need to create an *instance*.  You do this 
by running ``initdb``.  If you have your ``PATH`` set correctly,
this should be found in ``$MYROOT/bin``.  You can do a ``which initdb`` to double-check this, if you wish.

.. code-block:: bash

   $ initdb -U root -P somePasswordForRoot

This will create a postgres *instance* and define a superuser named *root* in it with the password
*somePasswordForRoot*.  (I'd recommend using something else for a password.) Had you not set ``PGDATA`` 
and PGPORT, you'd also need to specify these on the command line, like so:

.. code-block:: bash
 
   $ initdb -D $MYROOT/pgdata -p 5433 -U root -P ...

Starting the PostgreSQL Server
------------------------------

15. You can now start this instance of the postgres server by running the ``pg_ctl`` command, which you should
find in your ``PATH`` under ``$MYROOT/bin`` along with the rest of the postgres binaries you built, earlier.

.. code-block:: bash
 
   $ pg_ctl -l $MYROOT/logs/postgres.log start

Notice that the log file is explicitly specified.  You can set another environment variable for this
if you don't feel like providing it on the command line.  The choice is yours.  You will, however, need to
say ``start`` to tell it to start up the server.   If start-up succeeds you should see a message saying

.. code-block:: bash

  database system is ready to accept connections


16. Test your postgres installation by creating an account for yourself and a database to go with that account:

.. code-block:: bash

   $ createuser -U root -W $LOGNAME

This will create a postgres user with the same username as your current login name.  If you'd 
rather use some other name, supply that instead of ``$LOGNAME``.

This will prompt you for the (postgresql) root password you created in step 14.   You'll then be asked if 
this new user is to be a superuser.  You can answer yes or no, depending on how paranoid you are about security.
for the moment, answer "yes".

Next, create a default database for this user.  This will allow you to run the ``psql`` CLI without any 
arguments.

.. code-block:: bash

   $ createdb $LOGNAME
   $ psql

The ``createdb`` will produce no output.  When you run psql, you should see a version line in the output that
tells you what version of the CLI  you're running.  This should agree with the version you think you installed
(9.0.1, here.)

Installing Setuptools / easy_install
------------------------------------

17. You'll now want to install ``easy_install``, which will make the rest of the installation process go
much easier and faster.   First, see if it has already been installed by typing:

.. code-block:: bash

   $ which easy_install

If the response indicates that it's in your ``$MYROOT/bin`` directory, then you already have it installed. 
If not, grab the :ref:`easy_install bootstrap here <http://peak.telecommunity.com/dist/ez_setup.py>` and 
put it in your ``$MYROOT/src`` directory.

  $ cd $MYROOT
  $ curl -O "http://peak.telecommunity.com/dist/ez_setup.py"
  $ python ez_setup.py

This will install the distutils package which includes ``easy_install``.


Virtual Environments: Installing ``virtualenv``
-----------------------------------------------

18.  Install ``virtualenv``, the python utility for creating self-contained, virtual environments.  

.. code-block:: bash

   $ easy_install virtualenv

19. Create a new virtual environment somewhere in your account.  This is where we're going to build the 
pylons runtime environment, so let's call it ``pylons-rt`` ...

.. code-block:: bash

   $ virtualenv --no-site-packages $HOME/pylons-rt

Notice we said ``no-site-packages``.  This is because we want this new pylons environment to be pristine and
have no packages imported from (linked to, actually) our "root" python installation.  We're going to force
easy_install to get private copies of all packages ... including itself!   If you look in the ``bin``
directory of this new "environment" you'll see several executables, including ``python``, and ``easy_install``.
From now on it'll be VERY important to use THIS ``easy_install`` to install packages into this new
environment.  We'll set an environment variable to distinguish this area from ``$MYROOT`` and call it
``$PYLONSRTHOME`` ...

.. code-block:: bash

   $ export PYLONSRTHOME=$HOME/pylons-rt
   
We won't add ``$PYLONSRTHOME/bin`` to our ``PATH`` since we may not want that bin to be our default. 
This is one of those cases where explicit is better than implicit.

Installing Pylons et alia
-------------------------

20. Install the actual pylons runtime packages and several others that it'll need, into this virtualenv:

.. code-block:: bash

   $ $PYLONSRTHOME/bin/easy_install psycopg2 
   $ $PYLONSRTHOME/bin/easy_install sqlalchemy 
   $ $PYLONSRTHOME/bin/easy_install cfgconfig 
   $ $PYLONSRTHOME/bin/easy_install pylons 
   $ $PYLONSRTHOME/bin/easy_install authkit


21. Once this has finished, you should be ready to downlaod and run the ASCC portal code.  To get this, you'll 
need to have :ref:`mercurial` installed.  Use your "root" ``easy_install`` command to to install this
if you haven't already:

.. code-block:: bash

   $ easy_install mercurial

Test to see that merurial has now been installed by running the ``hg`` command by itself.  You should see a help
message.  If all has gone well, you should now be ready to clone a repository.

22. Change your working directory to wherever you'd like the ASCC portal code to go.  Let's say you have
a subdirectory of you ``$HOME`` called ``work``, so ``cd`` there and do an ``hg clone`` to get the code:

.. code-block:: bash

   $ cd $HOME/work
   $ hg ssh://asccdeveloper@ncascr.griffith.edu.au//mercurial/ASCC

This will create an ``ASCC`` subdirectory under ``work``.  Set the working directory to the top
of the ``Portal`` tree and run pylons:

.. code-block:: bash

   $ cd ASCC/Portal
   $ $PYLONSRTHOME/bin/paster serve --reload development.ini

This should now start up the pylons server for the ASCC portal.   If successfull, you should be
able to point your browser at  ``http://localhost:5050/`` and get the home page for the portal.

23.   If  you have questions or problems, don't hesitate to email me and let me know. This document
is currently in development and still needs testing and adjustment.  Your help is appreciated in this.

Nick Seidenman <seidenman@wehi.edu.au>


Supplemental
------------

If you're building in a Mac OS/X environment, chances are you won't find the :ref:`readline`
libraries at all or they'll be someplace not findable to the ``configure`` scripts.  Here are the
steps for downloading and adding readline to the configure/make processes, above. 

**DO THIS BEFORE GETTING AND BUILDING PYTHON OR POSTGRES!!**

1. Get a copy of the readline sources:

.. code-block:: bash

   $ cd $MYROOT/src
   $ curl -O "http://ftp.gnu.org/gnu/readline/readline-6.1.tar.gz"

2. Untar the archive and cd into the resulting directory:

   $ tar xzf readline-6.1.tar.gz
   $ cd readline-6.1

3. Run the configuration script, telling it what the prefix or root is:

   $ ./configure --prefix=$MYROOT
   $ make
   $ make install

4. Now you can build python and postgres as instructed, above, only add ``--with-readline=$MYROOT/src/readline-6.1`` to the configuration command.  

.. code-block:: bash

   # extract and cd into the python and postgres source directories and run
   # configure with the added --with-readline directive ...
   $ ./configure --prefix=$MYROOT --with-readline=$MYROOT/src/readline-6.1

All done







