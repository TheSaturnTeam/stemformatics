.. magma section

MAGMA Data Model
================

.. toctree:: 
   :maxdepth: 2

   annotation
   genome
   region
   registry
