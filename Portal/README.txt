Please note that you will need to "ln -s conf/development.ini config.ini" in the Portal root directory to create a symlink to the appropriate .ini file so you can run paste serve --reload config.ini in the Portal root directory

eg.
cd ASCC/Portal
ln -s conf/development.ini config.ini
paste server --reload config.ini

To see how Rowland got it working with Ubuntu see Portal/docs/Setup ASCC Portal.html



19/08/2010 - Rowland - the information below is currently out of date

This file is for you to describe the guide application. Typically
you would include information such as the information below:

Installation and Setup
======================

NOTICE:  These instructions will change and are apply only to the "embryonic" version
checked in on 15/4/2010 by nicks.  They include instructions for creating the r/t 
environment as well as setting up the app.  It is assumed that you have both virtualenv and
postgresql (8.3 or later) and, of couirse python (2.5 or later) installed.  Sqlite can be
be used in lieu of postgres FOR NOW.  Just be sure to change the userdb.url config item(s)
accordingly.  It also assumes you've cloned the repository, otherwise you wouldn't be reading
THIS.

Note for Mac installations.  The pre-built version of postgresql for MacOS doesn't play nicely
with psycopg2 mainly because it is built for 32-bit archs whereas psychopg2 and python 2.5 and
later default to 64-bit, though you can supposedly change this at run-time.  I tried, 
unsuccessfully, to do this and resorted to simply downloading and building the postgresql
client library(s) from source and installing them in a local (because they don't let us
have admin rights on our machines, here, so must do it all in mmm -- mere mortal mode)
"lib" directory which I then included in my LD_LIBRARY_PATH env var.  This works fine.
S'ok to still use the 32-bit server, though obviously performance is improved by using
64-bit all-around.  This appears to be a problem only with postgres -- I don't see similar
rants / complaints re: MySQL, e.g.

Note for everyone:  If you're using python 2.5 or later, DO NOT install sqlite3 separately.
It interferes with the sqlite3 module that is now included with the python distro causing
obscure, arcane, and maddeningly-manglesome diagnostics to appear, sending you off into
the wilds of Melbourne muttering nonsense.  (Yes, I speak from experience.)  Just ... 
don't.


1) virtualenv --no-site-packages portal-test

2) cd portal-potty

3) bin/easy_install pylons

4) bin/easy_install sqlalchemy

5) bin/easy_install authkit

6) bin/easy_install psycopg2

7) cp -r <ascc-repository-dir>/Guide .

8) cd Guide/guide/model

9) Depends on whether you're using sqlite3 or postgres:

   sqlite3:
       You shouldn't need to do anything.  The app is currently configured
       to use the sqlite version of the database.  If you need to re-create it
       for any reason:

           sqlite3 ../../data/users.db <users.sql

   postgres:
       create a database (and, if required, a role that is either pw-protected or
         has no pw.
       psql db-name-you-created <users.sql
       modify the portal.user.db.url config line in development.ini to reflect the
          db name and role/pw to use to access it.

   The sqlite version is the default found in the repository and works fine
   for our initial development efforts.  Eventually we'll want to move this
   over to postgres (or MySQL or oracle or ... but, I'd prefer pg) as I
   flesh out the clearance implementation.

9) ../bin/paste serve --reload development.ini

You should now be able to point your browser at http://localhost:5000/ and get a login 
page with a box where you can enter your openid.  If you don't have one, you can try putting 
in other, nonsensical stuff and it should give you an error message, taking you back to 
the login page.

When you first try to log in, you will be unlikely to succeed (I hope) since you won't
have openID url's registered in the subscriber db.  Once you get an openID url, you can
add it to the database by either adding an INSERT statement in the users.sql file and repeating
step 9, above, or just use the appropriate client to add a row to the users table in the db.
The users.sql file should make formats and content clear.

