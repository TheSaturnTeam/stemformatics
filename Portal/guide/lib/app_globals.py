"""The application's Globals object"""
import logging
log = logging.getLogger(__name__)
from pylons import config
from paste.deploy.converters import asbool
import json
from guide.model.stemformatics import *

class Globals(object):
    """Globals acts as a container for objects available throughout the
    life of the application

    """
    def __init__(self):
        """One instance of Globals is created during application
        initialization (ie. at server start-up) and is available during requests via the
        'app_globals' variable.
        
        In the controller, use:
        	from pylons import app_globals
        	print app_globals.someAttribute

		Note that this global variable is instantiated when server starts, and is shared amongst
		all connections to the server. Hence if a user changes such a variable from one browser, 
		another will see that change from another browser.
        """
        
        # the file is created by calling http://<base url>/expressions/setup_all_sample_metadata
        # it also sets up the g.all_sample_metadata in memory as well
        self.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()

       
        # The script portal-admin in stemformatics/scripts/sysadmin and stemformatics/sysadmin scripts
        # that runs a sql statement that downloads the gene_mappings.raw and the probe_mappings.raw
        # This task uses the gene_mappings.raw to manufacture the in memory list for quick access.
        # Get the probe mappings and gene mappings setup for bulk import manager
       
        if 'show_x_platform_graphing' in config:
            show_x_platform_graphing = asbool(config['show_x_platform_graphing'])
        else:
            show_x_platform_graphing = False



        """
        This is the only place I could find that runs purely on startup once.
        config is updated inside the function.
        """
        Stemformatics_Admin.trigger_update_configs()

        
