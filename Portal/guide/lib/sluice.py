
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class SluicedArgs(object):
    """\
Manages an argument list by `sluicing` it into different smaller lists
discriminated by their class.   This is mainly used by the ``__call__``
methods of the :ref:`ComposableQuery` subclasses to break out elements of
their ``*args`` into specific classes.

Example:

.. code-block:: python
 
 >>> # Instantiate a SluicedArge object with several diverse class instances ...
 
 >>> sa = SluicedArgs('this is a string', ['this', 'is', 'a', 'list'], {'somekey': 'this is a value'})
 
 >>> # Now retrieve the dictionary-like args ...
 
 >>> print = sa[dict]
 [{'somekey': 'this is a value'}]
 
 >>> # Retrieve string-like args (note, ca use either subscript or callable semantics)
 
 >>> print sa(type(''))
 ['this is a line']

 >>> # Retrieve whatever isn't a list- or tuple-like arg ...
 
 >>> print = sa(list, tuple, complement=True)
 ['this is a line', {'somekey': 'this is a value'}]


""" 

    def __init__(self, *args, **kwargs):
        self._sluice = dict()
        self._all = list()
        for a in args:
            cls = a.__class__
            s = self._sluice.get(cls, list())
            s.append(a)
            self._sluice[cls] = s
            self._all.append(a)
    
    def __getitem__(self, cls):
        return self.__call__(cls, complement=False)
    
    def __call__(self, *cls, **kwargs):
        complement = kwargs.get('complement', False)
        result = list()
        for s in self._sluice.keys():
            for c in cls:
                if issubclass(s, c):
                    result = result + self._sluice[s]
        if complement:
            all = [ a for a in list(self._all) if a not in result ]
            result = all
            
        return result
                
