"""Pylons environment configuration"""
import os

from mako.lookup import TemplateLookup
from pylons.error import handle_mako_error
from pylons import config
from sqlalchemy import engine_from_config

import guide.lib.app_globals as app_globals
import guide.lib.helpers

from guide.config.routing import make_map
from guide.model import init_model


def load_environment(global_conf, app_conf):
    """Configure the Pylons environment via the ``pylons.config``
    object
    """
    # Pylons paths
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    paths = dict(root=root,
                 controllers=os.path.join(root, 'controllers'),
                 static_files=os.path.join(root, 'public'),
                 templates=[os.path.join(root, 'templates')])

    # Initialize config with the basic options
    config.init_app(global_conf, app_conf, package='guide', paths=paths)

    config['routes.map'] = make_map()
    config['pylons.app_globals'] = app_globals.Globals()
    config['pylons.h'] = guide.lib.helpers

    # Create the Mako TemplateLookup, with the default auto-escaping
    config['pylons.app_globals'].mako_lookup = TemplateLookup(
        directories=paths['templates'],
        error_handler=handle_mako_error,
        module_directory=os.path.join(app_conf['cache_dir'], 'templates'),
        input_encoding='utf-8', output_encoding='utf-8',
        imports=['from webhelpers.html import escape'],
        default_filters=['escape'])
    
    # Setup SQLAlchemy database engine.
    #engine = engine_from_config(config, 'sqlalchemy.')
    #engine = None	# not using sqlalchemy

    init_model(config['app_conf'])
    
    # CONFIGURATION OPTIONS HERE (note: all config options will override
    # any Pylons config options)
    
    # Was getting strange __builtin__.unicode not mapped error
    # so did this as suggested by http://wiki.pylonshq.com/display/pylonsdocs/Unicode
    # ...
    # Don't coerce parameters to unicode
    # But, then I read this doc: http://wiki.pylonshq.com/display/pylonsdocs/Unicode
    # and saw that what we really want to do is set the charset (or change from
    # 'strict' handling to 'replace' or 'ignore'.)  For now we'll set the encoding to 
    # 'latin-1' and see how we go.
    # (Probably explains why Jarny had '# -*- coding: latin-1 -*-' At the top of all his
    # mako files.)
    config['pylons.request_options']['charset'] = 'utf-8'
    # You can also change the default error handler (strict, replace, ignore)
    #config['pylons.request_options']['errors'] = 'strict'

