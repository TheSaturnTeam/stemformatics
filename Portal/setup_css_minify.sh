#!/bin/bash
HOMEDIR=/home/rowlandm/s4m

cd $HOMEDIR/Portal/guide/public/css/tripoli
cat tripoli.base.css plugins/tripoli.visual.css plugins/tripoli.type.css tripoli.simple.css | yui-compressor --type=css > tripoli.full.min.css
cd $HOMEDIR/Portal/guide/public/css
cat ../themes/ui-lightness/jquery-ui-1.8rc3.custom.min.css cookiecontent.style.min.css tripoli/tripoli.full.min.css > combined_jquery_ui_tripoli_full.min.css
ls -alh combined_jquery_ui_tripoli_full.min.css
