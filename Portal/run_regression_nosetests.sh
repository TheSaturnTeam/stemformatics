#!/bin/bash
WORKING_DIR=/var/www/pylons/prod
NOSETESTS_DIR=/data/repo/git-working/stemformatics/regression_tests/nosetest_regression_tests/

source /var/www/pylons/virtualenv/bin/activate
cd $WORKING_DIR

nosetests --with-pylons=qfab-production-pglocal.ini  $NOSETESTS_DIR/*


