#!/bin/sh

echo "Sourcing virtualenv for 'pgloader'.."
source /var/www/pylons/virtualenv/bin/activate

# Create Database
DATABASE_NAME=production

echo "Dropping $DATABASE_NAME database.."
dropdb $DATABASE_NAME
echo "Creating $DATABASE_NAME database.."
createdb -O portaladmin $DATABASE_NAME

# Constructing Database Tables