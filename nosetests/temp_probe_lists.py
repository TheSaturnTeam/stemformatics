from pylons import request, response, session, url, tmpl_context as c
import json

# Live querying
from guide.model.stemformatics import *
from pylons import config



uid = 3
probe_list = ['p4@5end_chr15:96873892..96873919,+','p1@5end_chr10:103880075..103880177,-', 'chr10:104402674..104402693,+', 'p1@5end_chr10:105992059..105992138,-', 'p2@5end_chr10:106013923..106013945,+', 'p5@5end_chr10:116391770..116391838,-', 'p7@5end_chr10:116391923..116392002,-', 'chr10:118084968..118084984,+', 'p1@5end_chr10:118429755..118429805,-']



]
def test_1():

    result =  Stemformatics_Probe.set_probe_list(uid,probe_list)
    assert result == True

def test_2():

    result =  Stemformatics_Probe.get_probe_list(uid)
    assert len(result) == len(probe_list) 
    print result
    assert result[0] == 'p7@5end_chr10:116391923..116392002,-'
