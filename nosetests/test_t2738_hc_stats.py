import unittest
import logging
log = logging.getLogger(__name__)

import string
import json
from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config

from guide.model.stemformatics import *
from guide.model.graphs import *


def test_hc_stats():
    start_date = '2017-04-30'
    end_date = '2017-06-20'
    from guide.model.stemformatics.stemformatics_job import Stemformatics_Job
    result = Stemformatics_Job.get_hc_stats_from_s4m_db(start_date,end_date)
    assert result['selected']['datasets']['microarray'] == 34
    assert result['selected']['end_date'] == '2017-06-20'
    assert result['selected']['jobs'] == 143

def test_hc_stats_for_date_before_30_april(): #to check it only pics galaxy jobs
    start_date = '2017-04-15'
    end_date = '2017-07-25'
    from guide.model.stemformatics.stemformatics_job import Stemformatics_Job
    result = Stemformatics_Job.get_hc_stats_from_s4m_db(start_date,end_date)
    assert result['selected']['datasets']['microarray'] == 51
    assert result['selected']['users'] == 27
    assert result['selected']['jobs'] == 191
