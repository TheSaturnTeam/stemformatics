from guide.model.stemformatics import *
from guide.model.graphs import *
from pylons import config, request, session, tmpl_context as c

class tempData(object):
    pass


def test_hc_wizard():
    db = None
    db_id = 56
    ds_id = 5003
    gene_set_id = 2231
    result = Stemformatics_Gene_Set.get_probes_from_gene_set_id(db,db_id,ds_id,gene_set_id)



def _setup_graphs(temp_object):
    """ What other values are needed to be setup here for it to work?
    From expressions.py / _get_inputs_for_graph()
        self._temp.line_graph_available = Stemformatics_Dataset.check_line_graph_for_dataset(db,ds_id)
        self._temp.feature_type = feature_type
        self._temp.feature_id = feature_id
        self._temp.probeSearch = probeSearch
        self._temp.geneSearch = geneSearch
        self._temp.db_id = db_id
        self._temp.graphType = graphType
        self._temp.sortBy = sortBy
        self._temp.ds_id = ds_id
        self._temp.choose_dataset_immediately  = choose_dataset_immediately
        self._temp.url = request.environ.get('PATH_INFO')
        self._temp.original_temp_datasets = original_temp_datasets
        self._temp.force_choose = force_choose

        if request.environ.get('QUERY_STRING'):
            self._temp.url += '?' + request.environ['QUERY_STRING']
        self._temp.large = request.params.get('size') == "large"

    Note that lib/base.py / _check_dataset_status() and _check_gene_status
    are only affecting self._temp.db_id and temp_object.ref_id

    """
    species_dict = Stemformatics_Gene.get_species(db)

    ref_type = temp_object.ref_type
    ref_id = temp_object.ref_id
    graphType = temp_object.graphType
    sortBy = temp_object.sortBy
    if hasattr(temp_object,'select_probes'):
        select_probes = temp_object.select_probes
    else:
        select_probes = None

    list_of_samples_to_remove = []
    line_graph_available = temp_object.line_graph_available
    """ Build the graph data first using the temp_object and other information. And then choose the
    graph that is appropriate and then convert the data to be ready for the view """
    this_graph_data = Graph_Data(db,temp_object.ds_id,ref_type,ref_id,temp_object.db_id,list_of_samples_to_remove,species_dict,select_probes)

    if graphType == "scatter":
        this_graph = Scatterplot_Graph(this_graph_data,sortBy)
    if graphType == "box":
        this_graph = Box_Graph(this_graph_data,sortBy)
    if graphType =="bar":
        this_graph = Bar_Graph(this_graph_data,sortBy)
    if graphType =="line":
        this_graph = Line_Graph(this_graph_data,sortBy)



    this_view = Preview(this_graph,line_graph_available)
    return this_view


def test_template():
    db_id = 56
    gene_annotation_names_required = "no"
    ds_id = 6461
    gene_id_list = ['ENSG00000115425']
    result = Stemformatics_Gene_Set.get_probes_from_genes(db_id,ds_id,gene_id_list,gene_annotation_names_required)
    assert result == [[u'ILMN_1814120'], {u'ILMN_1814120': u'ENSG00000115425'}]



def test_setup_graph():
    _temp = tempData()
    ensembl_gene_id = 'ENSG00000166523'
    _temp.db_id = 56
    _temp.geneSearch =  ensembl_gene_id
    _temp.graphType = "bar"
    _temp.sortBy = "Sample Type"
    _temp.symbol = 'abc'
    _temp.ds_id = 6837
    _temp.choose_dataset_immediately  = False
    _temp.url = 'www.google.com'
    _temp.original_temp_datasets = []
    _temp.force_choose = False
    _temp.large = False
    _temp.dataset_status = True
    _temp.ref_type = 'ensemblID'
    _temp.ref_id = _temp.ensemblID = ensembl_gene_id
    _temp.line_graph_available = False
    _temp.this_view = _setup_graphs(_temp)


def test_setup_graph_2():
    _temp = tempData()
    ensembl_gene_id = 'ENSMUSG00000002633'
    _temp.db_id = 46
    _temp.geneSearch =  ensembl_gene_id
    _temp.graphType = "bar"
    _temp.sortBy = "Sample Type"
    _temp.symbol = 'abc'
    _temp.ds_id = 7127
    _temp.choose_dataset_immediately  = False
    _temp.url = 'www.google.com'
    _temp.original_temp_datasets = []
    _temp.force_choose = False
    _temp.large = False
    _temp.dataset_status = True
    _temp.ref_type = 'ensemblID'
    _temp.ref_id = _temp.ensemblID = ensembl_gene_id
    _temp.line_graph_available = False
    _temp.this_view = _setup_graphs(_temp)

    assert _temp.this_view == True
