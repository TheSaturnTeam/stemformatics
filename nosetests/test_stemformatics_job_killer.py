import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

log.debug('Testing in guide/model/stemformatics/test_stemformatics_gene_set.py')

human_db_id = 56
mouse_db_id = 46
gene_set_description_default = "THis is a test"

probe_default = ""
gene_default = ""

def test_return_all_analysis():
    resultData =  Stemformatics_Job.return_all_analysis() 
    print resultData
    assert resultData == {0: {'name': 'Hierarchical Cluster', 'description': 'Hierarchical clustering groups genes and samples to highlight co-regulated gene sets.'}, 1: {'name': 'Comparative Marker Selection', 'Comparative marker selection shows you which of your genes of interest are most differentially expressed in distinct phenotypes within a study.': 'CMS'}, 2: {'name': 'Gene Neighbourhood', 'This analysis will find genes that share a similar expression profile for your gene of interest across samples within a given study.': 'CMS'}, 4: {'This will annotate your gene set.': 'GSA', 'name': 'Gene Set Annotation'}}



def test_return_all_status():
    
    import os, subprocess
    
    StemformaticsController = "/home/s2776403/workspace/stemformatics/Portal/guide/controllers/StemformaticsController.jar"
    
    job_id = 190
    grep_string = StemformaticsController +" " + str(job_id)
    command = "kill -9 `ps aux | grep \""+grep_string+"\" | awk '{print $2}'`" 
    print command
    p = subprocess.Popen(command,shell=True)
    assert True == False
                




