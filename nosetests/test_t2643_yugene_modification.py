
import string
import json
from guide.model.stemformatics import *

# this test the breakdown_dict and full_data_dict
def test_full_data_dict():
    db_id = 56
    ensembl_id = 'ENSG00000229094'
    uid = 629
    full_data = Stemformatics_Expression.get_yugene_full_data_graph_values(uid,ensembl_id,db_id)
    filters = {"filter_value_start":"0.96872238079","filter_value_end":"0.99258755049"}
    metadata_list = 'Generic sample type'
    metadata_list = metadata_list.split(",")
    all_sample_metadata =  Stemformatics_Expression.setup_all_sample_metadata()

    result = Stemformatics_Expression.return_full_data_dict_and_filter_data_dict(filters,full_data,metadata_list,all_sample_metadata)
    breakdown_dict  = result[0]
    full_data_dict = result[1]
    print(result)
    assert len(breakdown_dict['Generic sample type']['iPS']['datasets']) == 1
    assert breakdown_dict['Generic sample type']['iPS']['datasets'][5036]['5445316039_E'] == 73 # 5445316039_E is chip_id and 73 is corresponding chip type
    assert full_data_dict['Generic sample type']['iPS']['value'] == 12
    assert breakdown_dict['Generic sample type']['iPS']['value'] == 12
    assert len(breakdown_dict['Generic sample type']['iPS']['datasets'][5036]) == 12
#
# this test breakdown dict return and save in redis
def test_filter_yugene_graph_method():
    db_id = 56
    ensembl_id = 'ENSG00000229094'
    uid = 629
    filters = {"filter_value_start":"0.96872238079","filter_value_end":"0.99258755049"}
    full_data = Stemformatics_Expression.get_yugene_full_data_graph_values(uid,ensembl_id,db_id)
    metadata_list = 'Generic sample type'
    all_sample_metadata =  Stemformatics_Expression.setup_all_sample_metadata()
    metadata_list = metadata_list.split(",")
    max_length = None
    max_length_action = 'truncate'

    result = Stemformatics_Expression.filter_yugene_graph(filters,db_id,full_data,metadata_list, all_sample_metadata,max_length,uid,ensembl_id, max_length_action)
    print(result)
    assert len(result['Generic sample type']['iPS']['datasets']) == 1
    assert result['Generic sample type']['iPS']['value'] == 12
    assert result['Generic sample type']['iPS']['full_count'] == 12

# this test breakdown dict retrieval from redis
def test_breakdown_data_saved_in_redis():
    db_id = '56'
    ensembl_id = 'ENSG00000229094'
    uid = 629
    filters = {"filter_value_start":"0.96872238079","filter_value_end":"0.99258755049"}
    filter_value_start = float(filters['filter_value_start'])
    filter_value_end = float(filters['filter_value_end'])
    result = Stemformatics_Expression.get_breakdown_dict_from_redis(uid,ensembl_id,db_id,filter_value_start,filter_value_end)
    assert len(result['Generic sample type']['iPS']['datasets']) == 1
    assert result['Generic sample type']['iPS']['value'] == 12

def test_get_handle_title_and_species_methods():
    ds_ids = [5003, 6253]
    result = Stemformatics_Dataset.get_handle_title_and_species(ds_ids)
    assert result[5003]['handle'] == 'Novershtern_2011_21241896'
    assert result[6253]['Organism'] == 'Homo sapiens'

def test_return_yugene_filtered_dataset_breakdown_method():
    uid = 629
    db_id = '56'
    ensembl_id = 'ENSG00000229094'
    filters = {"filter_value_start":"0.96872238079","filter_value_end":"0.99258755049"}
    filter_value_start = float(filters['filter_value_start'])
    filter_value_end = float(filters['filter_value_end'])
    breakdown_data = Stemformatics_Expression.get_breakdown_dict_from_redis(uid,ensembl_id,db_id,filter_value_start,filter_value_end)
    breakdown_data = breakdown_data['Generic sample type']['iPS']
    result = Stemformatics_Expression.return_yugene_filtered_dataset_breakdown(breakdown_data)
    print(result)
    assert result is not None
