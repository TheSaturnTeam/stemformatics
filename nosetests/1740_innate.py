from guide.model.stemformatics import *
from guide.templates import *

from pylons import config


uid = 3


def test_innate_form():

    single_gene_url = None
    multiple_gene_url = "http://www.innatedb.com/batchSearchInit.do"
    innate_db_object = innateDB(single_gene_url,multiple_gene_url)

    list_of_ensembl_ids = ['ENSG00000007372','ENSG00000073282','ENSG00000115461']
    html = innate_db_object.create_form_to_post(list_of_ensembl_ids)
    assert 'innate_db_type' in html
    assert list_of_ensembl_ids[0] in html
    assert list_of_ensembl_ids[1] in html
    assert list_of_ensembl_ids[2] in html

    dict_of_types = innate_db_object.return_dict_of_types()
    assert 'Gene Ontology Analysis' in dict_of_types
    assert dict_of_types['Gene Ontology Analysis'] == 'go'


    list_of_types_in_order = innate_db_object.return_list_of_types_in_order()
    assert 'Gene Ontology Analysis' in list_of_types_in_order
    assert list_of_types_in_order[3] == 'Interactor Analysis'

def test_string_url():

    single_gene_url = "http://string-db.com/newstring_cgi/show_network_section.pl?identifier=ENSG00000229094"
    string_db_object = stringDB(single_gene_url)

    ensembl_id = 'ENSG00000007372'
    url = string_db_object.get_single_gene_url(ensembl_id)
    assert url == single_gene_url+ensembl_id

    ensembl_id = u'ENSG00000007372'
    url = string_db_object.get_single_gene_url(ensembl_id)
    assert url == single_gene_url+ensembl_id
