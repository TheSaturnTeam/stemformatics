from guide.model.stemformatics import *
all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()

def test_metadata():
    ds_id = 6008
    chip_type = Stemformatics_Dataset.getChipType(db,ds_id)
    sort_by = 'Sample Type'
    sample_labels = Stemformatics_Expression.get_sample_labels(ds_id)
    assert chip_type == 46
    sample_chip_ids_in_order = Stemformatics_Dataset.get_sample_chip_ids_in_order(db,chip_type,sample_labels,sort_by,ds_id)

