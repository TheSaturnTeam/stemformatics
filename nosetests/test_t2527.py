import unittest
import logging
log = logging.getLogger(__name__)

import string
import json
from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config

from guide.model.stemformatics import *
from guide.model.graphs import *

species_dict = Stemformatics_Gene.get_species(db)

def test_check_when_gene_search_empty():
    db_id = 56
    gene = ""
    result = Stemformatics_Gene.check_number_of_gene_id_found(db_id,species_dict,gene)
    print result
    assert result == '0'

def test_check_getting_genes():
    ds_id = 2000
    db_id = 56
    gene = "ENSG00000115415"
    result = Stemformatics_Gene.get_genes(db, species_dict, gene, db_id, False, None)
    assert result is not None

def test_check_when_gene_not_found():
    db_id = 56
    gene = "ENSG00000115888"
    result = Stemformatics_Gene.check_number_of_gene_id_found(db_id,species_dict,gene)

    assert result is '0'

def test_check_when_gene_is_found():
    db_id = 56
    gene = "ENSG00000115415"
    result = Stemformatics_Gene.check_number_of_gene_id_found(db_id,species_dict,gene)
    assert result is not None

def test_to_get_expression_graph_data_from_database():
    ds_id = 1000
    probe_list = ['ILMN_1738956']
    db_id = 46
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    probe_expression_rows = Stemformatics_Expression.get_expression_rows(ds_id,probe_list)
    chip_type = Stemformatics_Dataset.getChipType(db,ds_id)
    sample_labels = Stemformatics_Expression.get_sample_labels(ds_id)
    probe_mapping_data = Stemformatics_Probe.get_multi_mapping_for_probes(probe_list,db_id,chip_type)
    multi_mapped_genes = probe_mapping_data[0]
    result = Stemformatics_Expression.get_expression_graph_data_from_database(ds_id,probe_expression_rows,sample_labels,chip_type,{'ILMN_1738956':"no"},multi_mapped_genes)
    assert result['ILMN_1738956'][5]['ds_id'] == '1000'

def test_to_get_expression_graph_data_from_database_when_limitsortby_have_different_values():
    ds_id = 6062
    probe_list = ['10453857']
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    probe_expression_rows = Stemformatics_Expression.get_expression_rows(ds_id,probe_list)
    chip_type = Stemformatics_Dataset.getChipType(db,ds_id)
    sample_labels = Stemformatics_Expression.get_sample_labels(ds_id)
    db_id= 46
    probe_mapping_data = Stemformatics_Probe.get_multi_mapping_for_probes(probe_list,db_id,chip_type)
    multi_mapped_genes = probe_mapping_data[0]
    result = Stemformatics_Expression.get_expression_graph_data_from_database(ds_id,probe_expression_rows,sample_labels,chip_type,{'10453857':1L},multi_mapped_genes)
    assert result['10453857'][5]['ds_id'] == '6062'

def test_to_get_probe_data():
    ds_id = 1000
    probe_list = ['ILMN_1745788']
    db_id = '56'
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    result = Stemformatics_Expression.get_expression_data_from_probes(probe_list,ds_id,db_id)
    assert result[0]['Probe'] == 'ILMN_1745788'
    assert result[1]['ds_id'] == '1000'

def test_to_get_probes_from_gene_list():
    ds_id = 2000
    gene_list = ["ENSG00000233911","ENSG00000042980"]
    db_id = '56'
    chip_type = Stemformatics_Dataset.getChipType(db,ds_id)
    gene_name_reqd = "no"
    result = Stemformatics_Gene_Set.get_probes_from_genes(db_id,chip_type,gene_list,gene_name_reqd)
    assert result[1]['ILMN_1665632'] == 'ENSG00000233911'

def test_get_mapping_for_genes():
    ds_id = 2000
    gene_list = ['ENSG00000000971','ENSG00000185499','ENSG00000005020']
    db_id = '56'
    result = Stemformatics_Gene.get_mapping_for_genes(gene_list,ds_id,db_id)
    assert result[1]['ENSG00000000971'] == [u'ILMN_1698144', u'ILMN_1657803', u'ILMN_1810910']
    assert result[1]['ENSG00000005020'] == [u'ILMN_1657129']

def test_to_get_graph_data_from_main_method_when_passing_gene():
    ds_id = 5003
    ref_id = ['ENSG00000163563','ENSG00000184481']
    db_id = '56'
    ref_type = 'ensemblID'
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    result = Stemformatics_Expression.get_expression_graph_data(ds_id,ref_id,ref_type,db_id)
    print result
    assert result[1]['ds_id'] == '5003'

def test_to_get_graph_data_from_main_method_when_passing_probe():
    ds_id = 2000
    ref_id = ['ILMN_1704512','ILMN_1656560','ILMN_1660027']
    db_id = '56'
    ref_type = 'probeID'
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    result = Stemformatics_Expression.get_expression_graph_data(ds_id,ref_id,ref_type,db_id)
    assert result[1]['ds_id'] == '2000'
    assert result[1]['Probe'] == 'ILMN_1660027'

def test_main_method_when_passing_multiple_gene_with_single_probes():
    ds_id = 2000
    ref_id = ['ENSG00000060656','ENSG00000071242']
    db_id = '56'
    ref_type = 'ensemblID'
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    result = Stemformatics_Expression.get_expression_graph_data(ds_id,ref_id,ref_type,db_id)
    assert result[0]['ds_id'] == '2000'
    assert result[0]['Probe'] == 'ILMN_1790801'
#
def test_main_method_when_passing_multiple_gene_with_multiple_probes():
    ds_id = 2000
    ref_id = ['ENSG00000182324','ENSG00000071242']
    db_id = '56'
    ref_type = 'ensemblID'
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    result = Stemformatics_Expression.get_expression_graph_data(ds_id,ref_id,ref_type,db_id)
    assert result[0]['ds_id'] == '2000'

def test_get_mapping_data_from_gene_set_id():
    ref_id = ['1790']
    ref_type = 'gene_set_id'
    db_id = 56
    ds_id =  6461
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    result = Stemformatics_Gene.get_mapping_for_gene_set(ref_id,db_id,ds_id)
    assert result['1790'] == [u'ENSG00000168291', u'ENSG00000236178', u'ENSG00000131828', u'ENSG00000196305', u'ENSG00000011376', u'ENSG00000067704', u'ENSG00000196305', u'ENSG00000133706', u'ENSG00000105552', u'ENSG00000163114', u'ENSG00000131828', u'ENSG00000060982', u'ENSG00000231945', u'ENSG00000231945', u'ENSG00000163114', u'ENSG00000236178']

def test_main_method_when_passing_gene_set():
    ref_id = ['1857']
    ref_type = 'gene_set_id'
    db_id = 56
    ds_id =  5005
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    result = Stemformatics_Expression.get_expression_graph_data(ds_id,ref_id,ref_type,db_id)
    assert result[0]['ds_id'] == '5005'

def test_stemformatics_gene_set_method_to_get_probes_from_genes():
    ds_id = 2000
    gene_list = ['ENSG00000060656','ENSG00000071242']
    db_id = '56'
    chip_type = Stemformatics_Dataset.getChipType(db,ds_id)
    result = Stemformatics_Gene_Set.get_probes_from_genes(db_id,chip_type,gene_list,"no")
    assert result[0] == [u'ILMN_1790801', u'ILMN_1657128', u'ILMN_1702501', u'ILMN_1716218']
    assert result[1] == {u'ILMN_1790801': u'ENSG00000071242', u'ILMN_1657128': u'ENSG00000060656', u'ILMN_1702501': u'ENSG00000071242', u'ILMN_1716218': u'ENSG00000071242'}


def test_change_graph_data_format_to_tsv():
    graph_data = [{'chip_Id': 'GSM686285', 'Standard_Deviation': '0', 'Probe': '10453857', 'ds_id': '6062', 'Multi_Mapping': 'no', u'Sample_Type': u'sertoli cells (13.5 dpc)', u'Gender': u'male', 'Expression_Value': '8.58768', 'Sample_ID': u'Gonad, Embryo 13.5 dpc male Sertoli cells rep3', 'LineGraphGroup': None, 'Day': None}, {'chip_Id': 'GSM686286', 'Standard_Deviation': '0', 'Probe': '10453857', 'ds_id': '6062', 'Multi_Mapping': 'no', u'Sample_Type': u'whole P0 mouse', u'Gender': u'NULL', 'Expression_Value': '7.40257', 'Sample_ID': u'P0 mouse (from Steve Potter) rep1', 'LineGraphGroup': None, 'Day': None}]
    format_type = "tsv"
    result = Stemformatics_Expression.change_graph_data_format(graph_data, format_type)
    assert result == 'Gender\tMulti_Mapping\tchip_Id\tStandard_Deviation\tProbe\tds_id\tSample_Type\tExpression_Value\tSample_ID\tLineGraphGroup\tDay\nmale\tno\tGSM686285\t0\t10453857\t6062\tsertoli cells (13.5 dpc)\t8.58768\tGonad, Embryo 13.5 dpc male Sertoli cells rep3\tNULL\tNULL\t\nNULL\tno\tGSM686286\t0\t10453857\t6062\twhole P0 mouse\t7.40257\tP0 mouse (from Steve Potter) rep1\tNULL\tNULL\t\n'
#
def test_change_graph_data_format_to_json():
    graph_data = [{'chip_Id': '1979409020_C', 'Standard_Deviation': 0, 'Probe': 'ILMN_1790801', 'ds_id': 2000, 'Sample_Type': u'Dermal Fibroblast', 'disease_state': 'control', 'Expression_Value': '3.14138378947', 'Sample_ID': u'fibroblast_control_donor_1', 'Day':'None','LineGraphGroup':'None'}, {'chip_Id': '1979409021_F', 'Standard_Deviation': 0, 'Probe': 'ILMN_1790801', 'ds_id': 2000, 'Sample_Type': u'Dermal Fibroblast', 'disease_state': 'control', 'Expression_Value': '3.51033373605', 'Sample_ID': u'fibroblast_control_donor_11', 'Day':'None','LineGraphGroup':'None'}]
    format_type = "json"
    result = Stemformatics_Expression.change_graph_data_format(graph_data, format_type)
    print result
    assert result == '[{"chip_Id": "1979409020_C", "Expression_Value": "3.14138378947", "disease_state": "control", "Standard_Deviation": 0, "Sample_ID": "fibroblast_control_donor_1", "Probe": "ILMN_1790801", "ds_id": 2000, "Day": "None", "Sample_Type": "Dermal Fibroblast", "LineGraphGroup": "None"}, {"chip_Id": "1979409021_F", "Expression_Value": "3.51033373605", "disease_state": "control", "Standard_Deviation": 0, "Sample_ID": "fibroblast_control_donor_11", "Probe": "ILMN_1790801", "ds_id": 2000, "Day": "None", "Sample_Type": "Dermal Fibroblast", "LineGraphGroup": "None"}]'

def test_to_get_line_graph_data_from_main_method_when_passing_gene():
    ds_id = 6126
    ref_id = ['ENSMUSG00000026565']
    db_id = '46'
    ref_type = 'ensemblID'
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    result = Stemformatics_Expression.get_expression_graph_data(ds_id,ref_id,ref_type,db_id)
    assert result[15]['ds_id'] == '6126'

def test_to_test_my_json_object_with_correct_json_object():
    ds_id = 5003
    ref_id = ['ENSG00000163563']
    db_id = '56'
    ref_type = 'ensemblID'
    pylons.app_globals._push_object(config['pylons.app_globals'])
    g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    data = Stemformatics_Expression.get_expression_graph_data(ds_id,ref_id,ref_type,db_id)
    result = Stemformatics_Expression.change_graph_data_format(data,"json")
    new_data =  json.loads(result)
    assert new_data[210]["chip_Id"] == "GSM609836_5202764005792184120204.D02"
    assert new_data[0]["chip_Id"] == "GSM609637_5202764005792185120204.G02"
    assert new_data[18]["chip_Id"] == "GSM609650_5202764005789146112904.E07"

def test_deleting_data_from_redis():
    result = Stemformatics_Expression.delete_data_from_redis(["probe_graph_data","gene_mapping_data","gene_set_mapping_data","dataset_metadata"])
    print result
    assert result is not None


def test_miRNA_mapping_data():
    ref_type = "miRNA"
    feature_id = "MI0005482"
    db_id = 46
    ds_id = 6128
    use_json = False
    species = Stemformatics_Dataset.returnSpecies(db_id)
    result = Stemformatics_Expression.get_mappings_for_miRNA(feature_id, species, ref_type, use_json,ds_id, db_id)
    assert result['MI0005482'] == ['mmu-mir-147-3p', 'mimat0017269', 'mmu-mir-147-5p', 'mmu-mir-147*', 'mimat0004857', 'mmu-mir-147']

def test_test_miRNA_mapping():
    ref_type = "miRNA"
    feature_id = "MI0005482"
    db_id = 46
    ds_id = 6128
    species = Stemformatics_Dataset.returnSpecies(db_id)
    use_json = False
    result = Stemformatics_Gene.find_feature_search_items(feature_id,species,ref_type,use_json)
    assert result == [{'description': 'Mus musculus miR-147 stem-loop', 'sequence': 'uaugaaucuaguggaaacauuucugcacaaacuagauguugaugccagugugcggaaaugcuucugcuacauuuguagg', 'symbol': 'mir-147', 'feature_id': 'MI0005482', 'feature_type': 'miRNA', 'species': 'Mus musculus', 'aliases': 'mmu-mir-147,MIMAT0017269,mmu-miR-147,MIMAT0004857,mmu-miR-147*,mmu-miR-147-3p,mmu-miR-147-5p'}]

def test_get_multi_mapping_data_for_probes():
    ds_id = 2000
    ref_id = ['ILMN_1704512','ILMN_1656560','ILMN_1660027']
    db_id = '56'
    ref_type = 'probeID'
    chip_type = Stemformatics_Dataset.getChipType(db,ds_id)
    result = Stemformatics_Probe.get_multi_mapping_for_probes(ref_id,db_id,chip_type)
    assert result[1]['ILMN_1704512'] == 2

def test_mapping_id_data():
    ds_list = [1000,2000,3000,4000,7011,5030]
    result = Stemformatics_Dataset.set_datasets_mapping_id_into_redis()
    assert result == True

def test_for_getting_dataset_data_from_database():
    ds_id = 2000
    result = Stemformatics_Dataset.get_expression_dataset_metadata_from_database(ds_id)
    assert result["detectionThreshold"] == "5.00"
    assert result["limitSortBy"] == "Sample Type,Disease State"

def test_to_set_dataset_data_in_redis():
    ds_id = 2000
    data = Stemformatics_Dataset.get_expression_dataset_metadata_from_database(ds_id)
    result = Stemformatics_Dataset.set_expression_dataset_metadata_into_redis(ds_id,data)
    assert result == True

def test_to_get_dataset_data_from_redis():
    ds_id = 2000
    result = Stemformatics_Dataset.get_expression_dataset_metadata_from_redis(ds_id)
    assert result['detectionThreshold'] == '5.00'
    assert result["limitSortBy"] == "Sample Type,Disease State"

def test_to_check_private_dataset():
    ds_id = 6083
    result = Stemformatics_Dataset.check_private_dataset(ds_id)
    assert result == 'True'

def test_dataset_data_integrated_function():
    ds_id = 2000
    result = Stemformatics_Dataset.get_expression_dataset_metadata(ds_id)
    assert result["detectionThreshold"] == "5.00"
    assert result["limitSortBy"] == "Sample Type,Disease State"
    assert result["medianDatasetExpression"] == "8.93"

def test_dataset_data_integrated_function():
    ds_id = 4000
    result = Stemformatics_Dataset.get_expression_dataset_metadata(ds_id)
    assert result["detectionThreshold"] == "5.00"
    assert result["limitSortBy"] == "Sample Type"
    assert result["medianDatasetExpression"] == "8.89"

def test_mapping():
    result = Stemformatics_Dataset.set_datasets_mapping_id_into_redis()
    assert result == True

def test_graph_colours_method():
    result = Stemformatics_Expression.get_colours_for_graph(0,0)
    print result
    assert result is not None
