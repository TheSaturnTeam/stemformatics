import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *


human_db_id = 56
mouse_db_id = 46



# tests addGeneSet, getGeneSets and getGeneSetData
def test_fisher_values():
    
    
    from fisher import pvalue
    # first row then second row left to right
    p = pvalue(96,97,547,27067)
    p = pvalue(48,319,4,36814-319)
    # print p.left_tail
    # print p.right_tail
    # print '%F' % p.right_tail
    # print '%G' % p.right_tail
    

    test = "7.27565235e-10"
    testing = float(test)
    
    # print testing < 7.26e-10
    
    test = "%.3g" % testing
    
    print p.left_tail
    print p
    
    
    assert p.left_tail == 0.994525206022
    # assert p.right_tail == 0.994525206022
    # assert p.two_tail == 0.0802685520741
