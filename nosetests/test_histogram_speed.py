from pylons import request, response, session, url, tmpl_context as c
from sqlalchemy import or_, and_, desc
from sqlalchemy.exceptions import *
import json
import logging
log = logging.getLogger(__name__)
from pylons import app_globals as g

# Live querying
from guide.model.stemformatics import *
from pylons import config
import redis

guest_username = config['guest_username']
db_user = Stemformatics_Auth.get_user_from_username(db,guest_username)
uid = db_user.uid
uid = 3
show_limited = True
def test():

    using_old()
    #using_new()
    #using_new(0)

def using_old():
    uid = 3
    gene_set_id = 290
    #Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id)
    db_id = 56
    ds_id = 5003
    sortBy = "sortBySample Type"
    ref_type = "gene_set_id"
    ref_id = "1843"
    all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata(db)
    Stemformatics_Expression.setup_all_single_gene_graph_details(db,db_id,ds_id,uid,sortBy,ref_type,ref_id,all_sample_metadata)
    #all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata(db)
    #setup_all_single_gene_graph_details(db,db_id,ds_id,uid,sortBy,ref_type,ref_id,all_sample_metadata)
    assert True == True 
    
def using_new():

    

    assert True == False


# This is actually for more than just single gene graphs - does also do the histogram
def setup_all_single_gene_graph_details(db,db_id,ds_id,uid,sortBy,ref_type,ref_id,all_sample_metadata):
            
    result = {} 
    
    # multi-mapping probe details
    chip_type = result['chip_type'] = Stemformatics_Dataset.getChipType(db,ds_id)
    
    # get the dataset metadata records for this function
    show_limited = True
    datasetMetadata = Stemformatics_Dataset.getExpressionDatasetMetadata(db,ds_id,uid,show_limited)
    if datasetMetadata == None:
        return 1
    
    species = datasetMetadata['species']
    
    result['detectionThreshold'] = datasetMetadata['detection_threshold']
    result['medianDatasetExpression'] = datasetMetadata['median_dataset_expression']
    result['datasetLimitSortBy'] = datasetMetadata['limit_sort_by']
    result['orderBySampleType'] = json.dumps(datasetMetadata['sampleTypeDisplayOrder'].split(','))
    
    # pick the first one if not found
    datasetLimitSortByArray = result['datasetLimitSortBy'].split(',')
    
    if sortBy is None:
        sortBy = "sortBySample Type"
    
    
    tempSortBy = sortBy.replace('sortBy','')    
    if tempSortBy not in datasetLimitSortByArray:
        sortBy = "sortBySample Type"
    
    result['sortBy'] = sortBy

    result['lineGraphOrdering'] = lineGraphOrdering= datasetMetadata['lineGraphOrdering']        
    result['lineGraphColours'] = json.dumps(Stemformatics_Expression.return_sample_type_display_group_colours(lineGraphOrdering))
    
    sampleTypeDisplayGroups = datasetMetadata['sampleTypeDisplayGroups']        
    result['sampleTypeDisplayGroupColours'] = json.dumps(Stemformatics_Expression.return_sample_type_display_group_colours(sampleTypeDisplayGroups))
    try:

        result['sampleTypeDisplayGroups'] = json.dumps(eval(datasetMetadata['sampleTypeDisplayGroups']))
    except:
        return 2
    
    
    # get the dataset name
    ds_handle = Stemformatics_Dataset.getHandle(db,ds_id,uid)
    if ds_handle == None:
        return 3
        
    result['datasetName'] = ds_handle
    
    if ref_type == 'miRNA' or ref_type == 'probeID' or ref_type == 'ensemblID':
        collapsed_data = Stemformatics_Expression.result_data(db,ref_type,ref_id,ds_id,True,db_id,all_sample_metadata)
        result['probe_information'] = json.dumps(Stemformatics_Probe.return_probe_information(db,ref_id,db_id,chip_type))
   

    if ref_type == 'gene_set_id':
        gene_set_id = ref_id 
        getData = Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id)
        ensemblList = [ gene_record.gene_id for gene_record in getData[1]]
        # ensemblList = ['ENSG00000115415','ENSG00000156508']
        
        gene_list = {}
        for gene_record in getData[1]:
            gene_list[gene_record.gene_id] = gene_record.associated_gene_name
            
        result['gene_list'] = json.dumps(gene_list)
        
        collapsed_data = multi_gene_result_data(db,ensemblList,ds_id,True,db_id,all_sample_metadata,sortBy)
        #collapsed_data = {} 
    if collapsed_data == None:
        return 4
    raise Error  
    result['collapsed_data'] = json.dumps(collapsed_data)
    
    
    # get information for change of datasets
    try:
        result['url'] = request.environ.get('PATH_INFO')
        if request.environ.get('QUERY_STRING'):
            result['url'] += '?' + request.environ['QUERY_STRING']
    except: 
        result['url'] = ''

    result['species'] = Stemformatics_Dataset.returnSpecies(db,db_id)
    result['list_authorised_users'] = json.dumps(Stemformatics_Dataset.list_authorised_users(db,ds_id))
    
    result['title'] = "Stemformatics - Expression for graph dataset " + result['datasetName']
    
    y_axis_result = Stemformatics_Expression.get_assay_platform_graph_info_from_ds_id(db,ds_id)
    
    result['min_y_axis'] = y_axis_result['min_y_axis']
    result['y_axis_label'] = y_axis_result['y_axis_label']
    result['y_axis_label_description'] = y_axis_result['y_axis_label_description']
    
    
    return result

def multi_gene_result_data(db,ensemblList,datasetID,useCollapsedData,db_id,all_sample_metadata,sortBy):
    #try:
    datasetID = int(datasetID)
    db_id = int(db_id)
    
    log.debug('result_data start')
    
        
    #filter for gene search and dataset id
    
    # join probe expressions and probe mappings
    log.debug('before query start')
    
    db.schema = 'stemformatics'
    
    db.schema = 'public'            
    datasetID = int(datasetID)
    db_id = int(db_id)
    
    # join probe expressions and probe mappings
    log.debug('before query start')
    
    db.schema = 'stemformatics'
    
    chip_type = Stemformatics_Dataset.getChipType(db,datasetID)
                
    db.schema = 'public'
    probe_list = []
    probe_mappings = {}
    sample_labels = Stemformatics_Expression.get_sample_labels(datasetID)
    returnList = {}
    for ensembl_id in ensemblList:
        result = Stemformatics_Gene_Set.get_probes_from_gene(db,db_id,chip_type,ensembl_id)
        new_probes = result[0]
        probe_list = probe_list + new_probes 
        for probe in new_probes:
            probe_mappings[probe] = ensembl_id
    
        # get the gct file
        probe_expression_rows = Stemformatics_Expression.get_expression_rows(datasetID,probe_list)
        returnList = {}
        count = 0
        metaDataList = {}
        for row in probe_expression_rows:
            
            #row_dict = gene_filtered_matricks_result.todict(row)
            probe_id = row
            sample_count = 0
            for expression_value in probe_expression_rows[row]:

                if expression_value != '' and expression_value != 'None':
                    expression_value = float(expression_value) 
                else:
                    expression_value = -999999999
                chip_id =  sample_labels[sample_count]
                sample_count += 1
                
                metaDataValues = all_sample_metadata[chip_type][chip_id][datasetID]
                sample_id = metaDataValues['Replicate Group ID']
                #standard_deviation = Stemformatics_Expression.get_standard_deviation(datasetID,chip_id,probe_id)
                standard_deviation = 0
                sampleID = sample_id
                sample_size = 1 # for now
            
                returnList[count] = {
                                "probeID": probe_id,
                                "expression_value": expression_value,
                                "standard_deviation": standard_deviation,
                                "chip_type": chip_type,
                                "sampleID": sampleID,
                                "ensemblID": probe_mappings[probe_id],
                                "sample_size": sample_size
                                 }       
                returnList[count][sortBy] = metaDataValues[sortBy.replace("sortBy","")]
                count = count + 1

    return returnList


