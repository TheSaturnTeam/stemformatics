from guide.model.stemformatics import *
from guide.model.graphs import *
import psycopg2
import psycopg2.extras


class tempData(object):
    pass

temp = tempData() 

def datasets_search(temp):
    uid = temp.uid
    search = temp.search
    rohart_msc = temp.rohart_msc
    filter_dict = {'show_limited':temp.show_limited,'rohart_msc_test':rohart_msc} 
    result = Stemformatics_Dataset.dataset_search(db,uid,search,filter_dict)

    return result

def test_datasets():
    temp.uid = 0
    temp.search = "ipsc and human"
    temp.rohart_msc = False
    temp.show_limited = False
    result = datasets_search(temp)
    assert 2000 not in result
    assert 5037 not in result
    assert len(result) > 10
    assert result[5027]['handle'] == 'Maherali_2008_18786420'
    assert result[5016]['cells_samples_assayed'] == 'hESC, iPSC, derivatives of 3-day endodermal induction of hESC and iPSC'

    temp.uid = 0
    temp.search = "ipsc and musculus"
    temp.rohart_msc = False
    temp.show_limited = False
    result = datasets_search(temp)
    assert 2000 not in result
    assert 5037 not in result
    assert len(result) > 0
    assert result[6080]['handle'] == 'Stadtfeld_2012_22387999'
    assert result[6080]['cells_samples_assayed'] == 'iPSC'

    temp.uid = 0
    temp.search = "jjjjjjjjj"
    temp.show_limited = False
    temp.rohart_msc = False
    result = datasets_search(temp)
    assert 2000 not in result
    assert 5037 not in result
    assert len(result) == 0

    temp.uid = 0
    temp.search = ""
    temp.show_limited = False
    temp.rohart_msc = False
    result = datasets_search(temp)
    assert 2000 in result
    assert 4000 in result
    assert len(result) > 100

    temp.uid = 0
    temp.search = "ipsc and musculus"
    temp.show_limited = False
    temp.rohart_msc = True
    result = datasets_search(temp)
    assert 2000 not in result
    assert 5037 not in result
    assert len(result) == 0


def test_json():

    temp_data = {}
    temp.uid = 0
    temp.search = "ipsc and musculus"
    temp.show_limited = False
    temp.rohart_msc = False
    datasets = datasets_search(temp)
    temp_data['order_of_datasets'] = {}

    data = {}

    for ds_id in datasets:
        temp_dict = {}
        temp_dict['organism'] = datasets[ds_id]['organism']
        temp_dict['name'] = datasets[ds_id]['handle']
        temp_dict['title'] = datasets[ds_id]['title']
        temp_dict['cells_samples_assayed'] = datasets[ds_id]['cells_samples_assayed']
               
        data[ds_id] = temp_dict
           
    json_data = json.dumps(data)
    assert len(json_data) > 100
