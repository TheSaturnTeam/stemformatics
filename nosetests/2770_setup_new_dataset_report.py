from guide.model.stemformatics import *
import psycopg2,psycopg2.extras,cPickle,subprocess
import pylons
from datetime import datetime , timedelta
import redis
import re
from pylons import config,app_globals as g
from pylons import url
pylons.app_globals._push_object(config['pylons.app_globals'])
g.all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()


def test_redis_ST_keys_list():
    list_of_changed_ds_ids = [6253]
    run_script_automatically = True
    info = Stemformatics_Expression.get_info_for_building_redis_short_term_keys(list_of_changed_ds_ids,db,run_script_automatically)
    ds_id_info = info[0]
    genes_info = info[1]
    gene_list_info = info[2]
    run_script_log = info[3]

    redis_keys_dict = Stemformatics_Expression.return_short_term_redis_keys(ds_id_info,genes_info,gene_list_info)

    assert redis_keys_dict is not None

def test_deleting_multi_gene_ST_redis_keys():
    gene_set_id = 4250
    result = Stemformatics_Gene_Set.delete_short_term_redis_keys_for_a_gene_list(gene_set_id)
    assert isinstance(result,bool)
