import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

mouse_db_id = 46
human_db_id = 56

""" Testing the gene controller"""

def test_1():
    return
    uid = 10
    gene_set_id = 97
    
    result = Stemformatics_Gene_Set.getGeneSetData(db,uid,gene_set_id)
    
    print len(result[1])
    
    assert True == False


def test_2():
    uid = 10
    gene_set_id = 97
    
    result = Stemformatics_Gene_Set.getGeneSetData_without_genome_annotations(db,uid,gene_set_id)
    
    main_filename = '/tmp/genome_annotations_56.txt'
    f = open(main_filename,'r')    
    
    gene_dict = {}
    for line in f:
        line = line.replace('\n','')
        row = line.split('|')
        gene_id = row[1].strip()
        description = row[2].strip()
        gene_dict[gene_id] = {'gene_id': gene_id, 'description': description}
    
    
    genes_in_gene_set = {}
    for gene_row in result[1]:
        gene_id = gene_row.gene_id
        genes_in_gene_set[gene_id] = gene_dict[gene_id]
    
    print genes_in_gene_set
    
    assert True == False
