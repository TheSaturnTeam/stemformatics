"""

    nosetests --with-pylons=config.ini guide/model/stemformatics/test_stemformatics_dataset.py


"""

from guide.model.stemformatics import *

from pylons import config

number_of_datasets = 249
true_list_of_ds_ids = [6144, 6145, 6146, 6147, 6149, 6150, 6151, 6152, 6153, 6154, 6155, 6156, 6157, 6158, 6159, 6160, 6161, 6162, 6163, 6164, 6062, 6166, 6167, 6168, 6169, 6170, 6063, 6172, 6173, 6064, 6179, 6180, 6181, 6182, 5041, 6184, 6185, 6186, 6049, 6066, 6190, 6193, 6194, 6195, 6197, 6198, 6199, 6201, 6203, 6206, 6207, 6208, 6213, 6216, 6218, 6222, 6223, 6072, 6228, 6229, 6231, 6237, 6238, 6242, 6075, 6245, 6247, 6252, 6254, 6264, 6265, 6267, 6268, 6270, 6165, 6272, 6273, 6274, 6275, 6276, 6277, 6278, 6280, 6284, 6288, 6289, 6290, 6291, 6292, 6293, 6296, 6297, 6300, 6303, 6304, 6305, 6306, 6307, 6308, 6309, 6310, 6312, 6313, 6315, 6316, 6317, 6318, 6320, 6321, 6325, 6326, 6328, 6329, 6332, 6334, 6338, 6339, 6343, 6344, 6345, 6346, 6347, 6349, 6352, 6353, 6355, 6356, 6358, 6359, 6360, 6362, 6366, 6368, 6370, 6372, 6382, 6390, 6393, 6271, 6399, 6402, 6403, 6405, 6413, 6414, 6415, 6416, 6111, 5024, 6000, 6001, 6003, 6004, 6005, 6006, 6007, 6008, 6037, 6021, 5001, 5002, 5003, 5004, 5005, 5006, 6031, 5008, 6033, 5012, 5013, 5015, 5016, 5018, 5019, 5020, 5021, 5022, 5023, 4000, 5025, 5027, 5028, 5029, 5030, 5031, 5032, 5033, 5035, 5036, 5037, 5038, 5039, 5040, 6065, 5042, 5043, 6068, 6070, 6071, 3000, 6074, 6047, 6076, 6079, 6080, 6081, 6082, 6083, 6084, 6085, 6086, 6087, 6088, 6089, 6135, 6092, 2000, 6098, 6051, 6101, 6104, 6052, 6106, 6107, 6108, 6110, 6053, 6114, 6395, 6117, 6118, 6119, 1000, 6121, 6123, 6124, 6125, 6126, 6127, 6128, 6130, 6131, 6120, 6134, 6057, 6136, 6138, 6139, 6140]

true_list_of_metadata_for_6268 = {'number_of_samples': 3, 'top_diff_exp_genes': u'ENSG00000140416,ENSG00000170558,ENSG00000106366,ENSG00000137801,ENSG00000039068', 'replicates': u'1/1', 'affiliation': u'Yale University School of Medicine, 10 Amistad Street, Room 401 D, New\nHaven, CT, 06520, USA.', 'ae_accession_id': u'NULL', 'sra_accession_id': 'N/A', 'accession_id': u'GSE21332', 'ena_accession_id': 'N/A', 'pub_med_id': u' 20618694', 'title': u'Global gene expression profile of human placental pericytes', 'show_yugene': True, 'platform': u'Illumina HumanWG-6 V3', 'show_limited': False, 'public_release_date': u'2014-12-31', 'email': u'jordan.pober@yale.edu', 'db_id': 56, 'handle': u'Maier_2010_20618694', 'description': u'Objective\u2014Pericytes are critical cellular components of the microvasculature that play a major\nrole in vascular development and pathologies, yet their study has been hindered by lack of a\nstandardized method for their isolation and growth. Here we report a method for culturing human\npericytes from a readily available tissue source, placenta, and provide a thorough characterization\nof resultant cell populations.\nMethods\u2014We developed an optimized protocol for obtaining pericytes by outgrowth from\nmicrovessel fragments recovered after enzymatic digestion of human placental tissue. We\ncharacterized outgrowth populations by immunostaining, by gene expression analysis, and by\nfunctional evaluation of cells implanted in vivo.\nResults\u2014Our approach yields human pericytes that may be serially expanded in culture and that\nuniformly express the cellular markers NG2, CD90, CD146, \u03b1-SMA, and PDGFR-\u03b2 , but lack\nmarkers of smooth muscle cells, endothelial cells, and leukocytes. When co-implanted with human\nendothelial cells into C.B-17 SCID/bg mice, human pericytes invest and stabilize developing\nhuman endothelial cell-lined microvessels.\nConclusions\u2014We conclude that our method for culturing pericytes from human placenta results\nin the expansion of functional pericytes that may be used to study a variety of questions related to\nvascular biology.', 'gene_pattern_analysis_access': u'<Disable/Allow (or leave blank for default enabled)>', 'publication_title': u'Global gene expression profile of human placental pericytes', 'geo_accession_id': u'GSE21332', 'private': False, 'top_miRNA': {}, 'authors': u'Maier C, Shepherd B, Pober J', 'pxd_accession_id': 'N/A', 'probes': u'48803', 'breakDown': 'N/A', 'probes detected': u'377', 'name': u'Jordan S. Pober', 'cells_samples_assayed': u'Pericytes', 'dataset_status': 'Available', 'project': u'', 'published': True, 'organism': u'Homo sapiens'}



def test_dataset_update():
    
    uid = 3
    datasets = Stemformatics_Dataset.getAllDatasetDetails(db,uid,False)

    list_of_ds_ids =  []
    for ds_id in datasets:
        list_of_ds_ids.append(ds_id) 

    assert len(datasets) == number_of_datasets

    assert list_of_ds_ids == true_list_of_ds_ids

    assert datasets[6268] == true_list_of_metadata_for_6268
