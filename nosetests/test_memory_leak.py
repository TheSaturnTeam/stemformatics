from pylons import request, response, session, url, tmpl_context as c
from sqlalchemy import or_, and_, desc
from sqlalchemy.exceptions import *
import json
import logging
log = logging.getLogger(__name__)
import redis
from pylons import app_globals as g

# Live querying
from guide.model.stemformatics import *
from pylons import config
import redis

def test(self):
    #result = test_memory_leak_redis_cli()
    #probe_expression_rows = test_memory_leak_expression_rows()
    #print probe_expression_rows
     
    assert True==False

    

def test_memory_leak_get_probes():
    db_id = 56
    chip_type = 41
    ref_id = "ENSG00000204531"
    for i in range (1,2):
        result = Stemformatics_Gene_Set.get_probes_from_gene(db,db_id,chip_type,ref_id)
    return result

def test_memory_leak_expression_rows():
    datasetID = 5003
    probe_list = [u'210905_x_at', u'210265_x_at', u'208286_x_at', u'214532_x_at']
    for i in range (1,10000):
        probe_expression_rows = Stemformatics_Expression.get_expression_rows(datasetID,probe_list)
    return probe_expression_rows 


