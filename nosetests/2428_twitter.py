from guide.model.stemformatics import *
from guide.templates import *
from guide.model import twitter

from pylons import config

uid = 3


def test_twitter():

    number = 3
    force_refresh = True
    result = twitter.get_recent_tweets(number,force_refresh)
    assert result[1] != {'type':'redis cache'}
    assert len(result[0]) == 3


    number = 3
    force_refresh = False
    result = twitter.get_recent_tweets(number,force_refresh)
    assert result[1] == {'type':'redis cache'}
    assert len(result[0]) == 3



    number = 5
    force_refresh = True
    result = twitter.get_recent_tweets(number,force_refresh)
    assert result[1] != {'type':'redis cache'}
    assert len(result[0]) == 5


    number = 3
    force_refresh = True
    result = twitter.get_recent_tweets(number,force_refresh)
    assert result[1] != {'type':'redis cache'}
    assert len(result[0]) == 3


