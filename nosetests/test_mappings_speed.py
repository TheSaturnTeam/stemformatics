from pylons import request, response, session, url, tmpl_context as c
from sqlalchemy import or_, and_, desc
from sqlalchemy.exceptions import *
import json
import logging
log = logging.getLogger(__name__)

# Live querying
from guide.model.stemformatics import *
from pylons import config
import redis

def test():
    set_mappings_into_redis()

def test_mapping_from_redis():

   
    assert True == True 
    

def set_mappings_into_redis():
    gene_mapping_raw_file_base_name = config['gene_mapping_raw_file_base_name']
    feature_mapping_raw_file_base_name = config['feature_mapping_raw_file_base_name']
    log.debug('starting mappings')
    result = Stemformatics_Gene.setup_bulk_import_manager_mappings(gene_mapping_raw_file_base_name,feature_mapping_raw_file_base_name)
    log.debug('finish mappings')

