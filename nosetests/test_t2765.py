import unittest
import logging
log = logging.getLogger(__name__)

import string
import json
from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config

from guide.model.stemformatics import *

def test_deleting_data_from_redis():
    result = Stemformatics_Expression.get_redis_data_stats(6124)
    print result
    assert result is not None
    # output will be something like
    # [['probe_graph_data|6124|ILMN_2655721|probeID|46', 'probe_graph_data|6124|ILMN_2593196|probeID|46'], ['gene_mapping_data|72|ENSMUSG00000026104|ensemblID|46'], [], [], ['dataset_metadata|6124']]
