from pylons import request, response, session, url, tmpl_context as c,config
import json
from guide.model.stemformatics import *

uid=3

def abc():
    
    analysis =0
    result = Stemformatics_Job.get_last_job_for_user(uid,analysis)
    assert result == 2127

    analysis =2
    result = Stemformatics_Job.get_last_job_for_user(uid,analysis)
    assert result == 2103

    analysis =4
    result = Stemformatics_Job.get_last_job_for_user(uid,analysis)
    assert result == 2102

    analysis =7
    result = Stemformatics_Job.get_last_job_for_user(uid,analysis)
    assert result == 2093



def test_get_gene_lists():

    gene_list_type = 'private'
    result = Stemformatics_Gene.get_last_gene_list(uid,gene_list_type) 
    assert result == 2170


    gene_list_type = 'public'
    result = Stemformatics_Gene.get_last_gene_list(uid,gene_list_type) 
    assert result == 1099

