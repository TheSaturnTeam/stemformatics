"""

    nosetests --with-pylons=config.ini guide/model/stemformatics/test_stemformatics_dataset.py


"""

from guide.model.stemformatics import *
from pylons import url,config

class TempClass:
    pass

def test_dataset_email():
    
    uid = 3
    ds_id = 6268
    dataset = Stemformatics_Dataset.getDatasetDetails(db,ds_id,uid)
    external_base_url = 'http://www.stemformatics.org/'
    username = "Rowland Mosbergen"

    dataset = Stemformatics_Dataset.getDatasetDetails(db,ds_id,uid)

    assert 'name' in dataset[ds_id]
    assert dataset[ds_id]['name'] == 'Jordan S. Pober'

    email = Stemformatics_Dataset.setup_email_to_contributing_author(dataset[ds_id],ds_id,username,external_base_url)

    assert email == True
