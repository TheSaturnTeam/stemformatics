#!/bin/bash
## ==========================================================================
## This script installs a new Python virtual environment with users' choice of
## Python version and then installs Pylons (and other modules) as required by
## the ASCC Portal.
##
## Author: O.Korn
## ==========================================================================

function Exit {
  exitCode="$1"
  if [ -z $exitCode ]; then
    exitCode=0
  fi
  exit $exitCode
}

function Answer_Yes {
  ans="$1"
  case $ans in
    y|Y    ) echo "true" ;;
    *      ) echo "false" ;;
  esac
}

function Debug_Trace {
  echo "DEBUG: $1"
}

function Check_Exists_EasyInstall {
  which easy_install > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    : ##skip
  else
    echo "Oops, can't locate 'easy_install' script in PATH. Please install and try again."
    echo "Exiting."
    Exit 1
  fi
}

function Install_Virtualenv {
  Check_Exists_EasyInstall
  echo "Invoking 'easy_install virtualenv'.."
  easy_install virtualenv 
}

function Check_Exists_Virtualenv {
  which virtualenv > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    : ##skip
  else
    echo -n "Oops, can't locate 'virtualenv' script in PATH, attempt to install it? [Y/n] "
    read ans
    if [ -z $ans ]; then
      ans="y"
    fi
    if [ `Answer_Yes $ans` = "true" ]; then
       Install_Virtualenv
    else
      echo "Aborting virtualenv installation and configuration."
      Exit 1
    fi
    echo "'virtualenv' is installed."; echo
  fi
}

function Print_Python_Executables_In_PATH {
  pathDirs=`echo $PATH | sed -e 's/\:/\ /g'`
  pathDirs="$pathDirs /usr/local/bin"
  foundPythonPaths=""
  for path in $pathDirs; do
    if [ `basename $path` = "bin" ]; then
      if [ -d $path ]; then
        pythonExecutables=`ls $path | grep -E ^python | grep -v \-`
        for pyexec in $pythonExecutables; do
          foundPythonPaths="$foundPythonPaths \
$path/$pyexec"
        done
      fi
    fi
  done
  foundPythonPaths=`echo $foundPythonPaths | uniq -`
  echo $foundPythonPaths
}

function Read_String {
  returnvar="$1"
  ans="n"
  while [ `Answer_Yes $ans` = "false" ]; do
    echo -n "> "
    read input 
    echo -n "You entered '$input', is that correct? [Y/n] "
    read ans
    if [ -z $ans ]; then
      ans="y"
    else
      echo "Please try again."
    fi
  done
  eval "export \$returnvar=\"$input\""
}

function Get_Python_Path {
  echo "Enter path to Python executable to install in your new virtual environment."
  echo "(Found the following from your PATH:)"
  Print_Python_Executables_In_PATH
  Read_String $1
}

function Get_Virtualenv_Path {
  echo "Enter path to virtual environment in which to install Pylons: "
  Read_String $1
}

function Create_Virtualenv {
  Check_Exists_Virtualenv
  Get_Virtualenv_Path "VirtualenvPath"
  Debug_Trace "calling Get_Python_Path.."
  Get_Python_Path "PythonPath"
  echo "Configuring new virtual environment.."
  virtualenv --no-site-packages --python=$PythonPath $VirtualenvPath
  if [ $? -ne 0 ]; then
    echo "Oops, virtualenv creation failed, aborting.."
    Exit 1
  fi
}

function Easyinstall_Package {
  package="$1"
  if [ -z "$EasyinstallPath" ]; then
    echo "Oops, easy_install path not set, aborting.."
    Exit 1
  fi
  echo "Installing package '$package' into virtual environment.."
  eval "\$EasyinstallPath \$package"
  if [ "$?" -ne 0 ]; then
    echo "WARNING: Oops, looks like installation of '$package' failed!"
    echo "Continuing with next package.."
  fi
}


## Install "pgloader" via egg from repository
function Install_PGLoader {
  cwd=`pwd`
  dirname=`dirname $PWD`
  parent=`basename $dirname`
  if [ $parent  = "ASCC" ]; then
    ## Assume that we're in a repository clone
    :
  else
    echo "Could not determine if ASCC repository available."
    echo "Skipping 'pgloader' installation.."
    echo "PLEASE manually install the pgloader \"egg\" into your virtual environment."
    echo "(Otherwise 'portal-admin' script may have a difficult time)"
  fi
}



## MAIN ##

PYTHON_PKG_INSTALL="pylons sqlalchemy authkit psycopg2"


if [ `whoami` != "root" ]; then
  echo "You must be root (or sudo) to run this script!"; echo
  Exit 1
fi

echo -n "Create a new Python virtual environment? [Y/n] "
read ans
if [ -z $ans ]; then
  ans="y"
fi
if [ `Answer_Yes $ans` = "true" ]; then
  PythonPath=""
  VirtualenvPath="" 
  Create_Virtualenv
  echo 
  echo "NOTE: To manually load virtual environment into shell, use command:"
  echo "   $ source $VirtualenvPath/bin/activate"; echo
else
  echo "..skipping creation of new virtualenv, use existing (enter below)."
fi
if [ -z $VirtualenvPath ]; then
  Get_Virtualenv_Path "VirtualenvPath"
fi

export EasyinstallPath="$VirtualenvPath/bin/easy_install"

for pkg in $PYTHON_PKG_INSTALL; do
  Easyinstall_Package $pkg
done

Install_PGLoader

echo "All done."
