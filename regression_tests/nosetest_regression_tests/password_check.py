from pylons import config
import re


def test_this():
    # 12 characters and with a space
    correct_validation_regex_text = '(?=^.{12,}$)(?=.*\s+).*$'
    password = 'I am the SCA'
    validation_regex_text = config['validation_regex']

    validation_regex = re.compile(validation_regex_text)
    m = validation_regex.match(password)

    assert validation_regex_text == correct_validation_regex_text
    assert m is not None
